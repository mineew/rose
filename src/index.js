import React, { Fragment, StrictMode } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import ErrorBoundary from './ErrorBoundary'
import App from './App'

/** @type {'strict'|'normal'} */
const REACT_APP_MODE = process.env.REACT_APP_MODE

function RenderApp () {
  const AppWrapper = REACT_APP_MODE === 'strict'
    ? StrictMode
    : Fragment

  return (
    <AppWrapper>
      <Provider store={store}>
        <ErrorBoundary>
          <App />
        </ErrorBoundary>
      </Provider>
    </AppWrapper>
  )
}

ReactDOM.render(
  <RenderApp />,
  document.getElementById('root')
)
