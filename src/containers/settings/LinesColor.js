import { TextInput } from '../../components'
import { setLinesColor } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  TextInput,
  (state) => state.linesColor,
  setLinesColor,
  {
    label: 'Цвет линий',
    type: 'color'
  }
)
