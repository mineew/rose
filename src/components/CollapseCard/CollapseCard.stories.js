import React from 'react'
import { storiesOf } from '@storybook/react'
import { text } from '@storybook/addon-knobs'
import { Alert } from 'antd'
import CollapseCard from './CollapseCard'

const stories = storiesOf('Components', module)

const error = (message) => (
  <Alert
    description={message}
    type="error"
    showIcon
  />
)

const loremIpsum = `
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
`.trim()

stories.add('CollapseCard', () => {
  const titleKnob = text('title', 'Title')
  const childrenKnob = text('children', loremIpsum)
  const iconKnob = text('icon', 'info')

  if (!titleKnob.trim()) {
    return error(<>The <code>title</code> prop is required</>)
  }

  if (!childrenKnob.trim()) {
    return error(<>The <code>children</code> prop is required</>)
  }

  return (
    <div style={{ margin: 20 }}>
      <CollapseCard title={titleKnob} icon={iconKnob}>
        {childrenKnob}
      </CollapseCard>
    </div>
  )
})
