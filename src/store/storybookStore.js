import { createStore } from 'redux'
import withReduxEnhancer from 'addon-redux/enhancer'
import rootReducer from '../reducers'

/** @type {import('redux').Store} */
const store = createStore(rootReducer, withReduxEnhancer)

export default store
