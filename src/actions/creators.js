import * as t from './types'

/**
 * @typedef {{type: string, payload?: any}} Action
 * @typedef {{type: string, payload: Error, error: boolean}} ErrorAction
 */

/// --- App Data Actions -------------------------------------------------------

/**
 * @param {Array<[string, string]>} points
 * @returns {Action}
 */
export function setDataPoints (points) {
  return {
    type: t.SET_DATA_POINTS,
    payload: points
  }
}

/**
 * @param {string} title
 * @returns {Action}
 */
export function setTitle (title) {
  return {
    type: t.SET_TITLE,
    payload: title
  }
}

/**
 * @param {string} subtitle
 * @returns {Action}
 */
export function setSubtitle (subtitle) {
  return {
    type: t.SET_SUBTITLE,
    payload: subtitle
  }
}

/**
 * @param {number} tickInterval
 * @returns {Action}
 */
export function setTickInterval (tickInterval) {
  return {
    type: t.SET_TICK_INTERVAL,
    payload: tickInterval
  }
}

/**
 * @param {number} step
 * @returns {Action}
 */
export function setStep (step) {
  return {
    type: t.SET_STEP,
    payload: step
  }
}

/**
 * @param {number} size
 * @returns {Action}
 */
export function setLabelsSize (size) {
  return {
    type: t.SET_LABELS_SIZE,
    payload: size
  }
}

/**
 * @param {string} color
 * @returns {Action}
 */
export function setLabelsColor (color) {
  return {
    type: t.SET_LABELS_COLOR,
    payload: color
  }
}

/**
 * @param {string} color
 * @returns {Action}
 */
export function setGridColor (color) {
  return {
    type: t.SET_GRID_COLOR,
    payload: color
  }
}

/**
 * @returns {Action}
 */
export function toogleShowLines () {
  return {
    type: t.TOOGLE_SHOW_LINES
  }
}

/**
 * @param {string} color
 * @returns {Action}
 */
export function setLinesColor (color) {
  return {
    type: t.SET_LINES_COLOR,
    payload: color
  }
}

/**
 * @param {number} width
 * @returns {Action}
 */
export function setLinesWidth (width) {
  return {
    type: t.SET_LINES_WIDTH,
    payload: width
  }
}

/**
 * @returns {Action}
 */
export function toogleShowAreas () {
  return {
    type: t.TOOGLE_SHOW_AREAS
  }
}

/**
 * @param {string} color
 * @returns {Action}
 */
export function setAreasBgColor (color) {
  return {
    type: t.SET_AREAS_BG_COLOR,
    payload: color
  }
}

/**
 * @param {number} opacity as a percentage
 * @returns {Action}
 */
export function setAreasOpacity (opacity) {
  return {
    type: t.SET_AREAS_OPACITY,
    payload: opacity
  }
}

/**
 * @param {string} color
 * @returns {Action}
 */
export function setAreasLineColor (color) {
  return {
    type: t.SET_AREAS_LINE_COLOR,
    payload: color
  }
}

/**
 * @param {number} width
 * @returns {Action}
 */
export function setAreasLineWidth (width) {
  return {
    type: t.SET_AREAS_LINE_WIDTH,
    payload: width
  }
}

/// --- Manage Actions ---------------------------------------------------------

/**
 * @returns {Action}
 */
export function fileOpenRequest () {
  return { type: t.FILE_OPEN_REQUEST }
}

/**
 * @returns {Action}
 */
export function fileOpenStart () {
  return { type: t.FILE_OPEN_START }
}

/**
 * @param {string} path
 * @param {string} name
 * @param {object} data
 * @returns {Action}
 */
export function fileOpenSuccess (path, name, data) {
  return {
    type: t.FILE_OPEN_SUCCESS,
    payload: { path, name, data }
  }
}

/**
 * @param {Error} error
 * @returns {ErrorAction}
 */
export function fileOpenError (error) {
  return {
    type: t.FILE_OPEN_ERROR,
    payload: error,
    error: true
  }
}

/**
 * @returns {Action}
 */
export function fileSaveAsRequest () {
  return { type: t.FILE_SAVE_AS_REQUEST }
}

/**
 * @returns {Action}
 */
export function fileSaveAsStart () {
  return { type: t.FILE_SAVE_AS_START }
}

/**
 * @param {string} path
 * @param {string} name
 * @param {object} data
 * @returns {Action}
 */
export function fileSaveAsSuccess (path, name, data) {
  return {
    type: t.FILE_SAVE_AS_SUCCESS,
    payload: { path, name, data }
  }
}

/**
 * @param {Error} error
 * @returns {ErrorAction}
 */
export function fileSaveAsError (error) {
  return {
    type: t.FILE_SAVE_AS_ERROR,
    payload: error,
    error: true
  }
}

/**
 * @returns {Action}
 */
export function fileSaveStart () {
  return { type: t.FILE_SAVE_START }
}

/**
 * @param {object} data
 * @returns {Action}
 */
export function fileSaveSuccess (data) {
  return {
    type: t.FILE_SAVE_SUCCESS,
    payload: { data }
  }
}

/**
 * @param {Error} error
 * @returns {ErrorAction}
 */
export function fileSaveError (error) {
  return {
    type: t.FILE_SAVE_ERROR,
    payload: error,
    error: true
  }
}

/**
 * @returns {Action}
 */
export function fileClose () {
  return { type: t.FILE_CLOSE }
}

/**
 * @returns {Action}
 */
export function importRequest () {
  return { type: t.IMPORT_REQUEST }
}

/**
 * @returns {Action}
 */
export function importStart () {
  return { type: t.IMPORT_START }
}

/**
 * @param {Array<[string, string]>} chartData
 * @returns {Action}
 */
export function importSuccess (chartData) {
  return {
    type: t.IMPORT_SUCCESS,
    payload: { chartData }
  }
}

/**
 * @param {Error} error
 * @returns {ErrorAction}
 */
export function importError (error) {
  return {
    type: t.IMPORT_ERROR,
    payload: error,
    error: true
  }
}

/**
 * @returns {Action}
 */
export function quit () {
  return { type: t.QUIT }
}
