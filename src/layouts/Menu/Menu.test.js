import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-testing-library'
import store from '../../store'
import Menu from './Menu'

describe('<Menu />', () => {
  it('renders', () => {
    render(
      <Provider store={store}>
        <Menu />
      </Provider>
    )
  })
})
