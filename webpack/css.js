const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')

/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @param {Configuration} base
 * @returns {Configuration}
 */
module.exports = (base) => {
  const isProduction = base.mode === 'production'

  const style = {
    loader: 'style-loader',
    options: {
      // source maps in dev mode don't work properly in Firefox
      // seems it is an issue with style-loader
      // https://github.com/webpack-contrib/style-loader/issues/303
      // TODO: need to try to fix this
      sourceMap: true
    }
  }

  const extract = {
    loader: MiniCssExtractPlugin.loader
  }

  const css = {
    loader: 'css-loader',
    options: {
      sourceMap: true,
      importLoaders: 1
    }
  }

  const postcss = {
    loader: 'postcss-loader',
    options: {
      ident: 'postcss',
      sourceMap: true
    }
  }

  const extractPlugin = isProduction && new MiniCssExtractPlugin({
    filename: 'css/[name].[contenthash].css'
  })

  /// --------------------------------------------------------------------------
  //
  // source maps can be excluded from the vendors bundle if necessary
  //
  // const minimizeSourcePlugin = isProduction && (
  //   new OptimizeCssAssetsWebpackPlugin({
  //     assetNameRegExp: /^((?!vendors).)*.css$/,
  //     cssProcessorOptions: {
  //       map: {
  //         inline: false,
  //         annotation: true
  //       }
  //     }
  //   })
  // )
  //
  // const minimizeVendorsPlugin = isProduction && (
  //   new OptimizeCssAssetsWebpackPlugin({
  //     assetNameRegExp: /vendors.*.css/
  //   })
  // )
  /// --------------------------------------------------------------------------

  const minimizePlugin = isProduction && new OptimizeCssAssetsWebpackPlugin({
    cssProcessorOptions: {
      map: {
        inline: false,
        annotation: true
      }
    }
  })

  return {
    module: {
      rules: [{
        test: /\.css$/,
        use: [isProduction ? extract : style, css, postcss]
      }]
    },
    plugins: [extractPlugin].filter(Boolean),
    optimization: {
      minimizer: [minimizePlugin].filter(Boolean)
    }
  }
}
