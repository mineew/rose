/// --- App Data Actions -------------------------------------------------------
export const SET_DATA_POINTS = 'SET_DATA_POINTS'

export const SET_TITLE = 'SET_TITLE'
export const SET_SUBTITLE = 'SET_SUBTITLE'
export const SET_TICK_INTERVAL = 'SET_TICK_INTERVAL'
export const SET_STEP = 'SET_STEP'
export const SET_LABELS_SIZE = 'SET_LABELS_SIZE'
export const SET_LABELS_COLOR = 'SET_LABELS_COLOR'
export const SET_GRID_COLOR = 'SET_GRID_COLOR'
export const TOOGLE_SHOW_LINES = 'TOOGLE_SHOW_LINES'
export const SET_LINES_COLOR = 'SET_LINES_COLOR'
export const SET_LINES_WIDTH = 'SET_LINES_WIDTH'
export const TOOGLE_SHOW_AREAS = 'TOOGLE_SHOW_AREAS'
export const SET_AREAS_BG_COLOR = 'SET_AREAS_BG_COLOR'
export const SET_AREAS_OPACITY = 'SET_AREAS_OPACITY'
export const SET_AREAS_LINE_COLOR = 'SET_AREAS_LINE_COLOR'
export const SET_AREAS_LINE_WIDTH = 'SET_AREAS_LINE_WIDTH'

/// --- Manage Actions ---------------------------------------------------------
export const FILE_OPEN_REQUEST = 'FILE_OPEN_REQUEST'
export const FILE_OPEN_START = 'FILE_OPEN_START'
export const FILE_OPEN_SUCCESS = 'FILE_OPEN_SUCCESS'
export const FILE_OPEN_ERROR = 'FILE_OPEN_ERROR'

export const FILE_SAVE_AS_REQUEST = 'FILE_SAVE_AS_REQUEST'
export const FILE_SAVE_AS_START = 'FILE_SAVE_AS_START'
export const FILE_SAVE_AS_SUCCESS = 'FILE_SAVE_AS_SUCCESS'
export const FILE_SAVE_AS_ERROR = 'FILE_SAVE_AS_ERROR'

export const FILE_SAVE_START = 'FILE_SAVE_START'
export const FILE_SAVE_SUCCESS = 'FILE_SAVE_SUCCESS'
export const FILE_SAVE_ERROR = 'FILE_SAVE_ERROR'

export const FILE_CLOSE = 'FILE_CLOSE'

export const IMPORT_REQUEST = 'IMPORT_REQUEST'
export const IMPORT_START = 'IMPORT_START'
export const IMPORT_SUCCESS = 'IMPORT_SUCCESS'
export const IMPORT_ERROR = 'IMPORT_ERROR'

export const QUIT = 'QUIT'
