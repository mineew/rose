import { NumberInput } from '../../components'
import { setStep } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  NumberInput,
  (state) => state.step,
  setStep,
  {
    label: 'Шаг подписей (через каждые)',
    min: 1,
    max: 30
  }
)
