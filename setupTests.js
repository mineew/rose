import { cleanup } from 'react-testing-library'

afterEach(cleanup)

const pathModuleMock = {
  basename (path) {
    return path.split('/')[path.split('/').length - 1]
  }
}

global.readFileError = null
global.readFileData = ''
global.writeFileError = null

const fsModuleMock = {
  readFile (_, __, callback) {
    if (global.readFileError) {
      callback(global.readFileError)
    } else {
      callback(null, global.readFileData)
    }
  },

  writeFile (_, __, callback) {
    if (global.writeFileError) {
      callback(global.writeFileError)
    } else {
      callback()
    }
  }
}

global.excelData = [
  ['90', '100'],
  ['45', '120'],
  ['180', '87']
]

const exceljsModuleMock = {
  Workbook: class {
    constructor () {
      this.xlsx = {
        readFile: () => Promise.resolve()
      }

      this.getWorksheet = () => ({
        getCell: jest.fn(() => ({ text: '' }))
          .mockImplementationOnce(() => ({ text: global.excelData[0][0] }))
          .mockImplementationOnce(() => ({ text: global.excelData[0][1] }))
          .mockImplementationOnce(() => ({ text: global.excelData[1][0] }))
          .mockImplementationOnce(() => ({ text: global.excelData[1][1] }))
          .mockImplementationOnce(() => ({ text: global.excelData[2][0] }))
          .mockImplementationOnce(() => ({ text: global.excelData[2][1] }))
      })
    }
  }
}

global.showOpenDialogPaths = ['/some/path']
global.showSaveDialogPath = '/some/path'

const dialogMock = {
  showOpenDialog: jest.fn((_, callback) => {
    callback(global.showOpenDialogPaths)
  }),
  showSaveDialog: jest.fn((_, callback) => {
    callback(global.showSaveDialogPath)
  })
}

const appMock = {
  quit: jest.fn()
}

global.electron = {
  remote: {
    require: jest.fn((name) => {
      if (name === 'path') {
        return pathModuleMock
      }
      if (name === 'fs') {
        return fsModuleMock
      }
      if (name === 'exceljs') {
        return exceljsModuleMock
      }
      return {}
    }),
    dialog: dialogMock,
    app: appMock
  }
}
