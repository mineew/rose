import { connect } from 'react-redux'
import { DataTable } from '../../components'
import { rawChartData } from '../../store/selectors'
import { setDataPoints } from '../../actions'

export function mapStateToProps (state) {
  return {
    value: rawChartData(state)
  }
}

export function mapDispatchToProps (dispatch) {
  return {
    onChange (changedDataPoints) {
      dispatch(setDataPoints(changedDataPoints))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DataTable)
