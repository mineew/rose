import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Alert } from 'antd'
import './ErrorBoundary.css'

class ErrorBoundary extends Component {
  constructor (props) {
    super(props)
    this.state = { error: null }
  }

  static getDerivedStateFromError (error) {
    return { error }
  }

  render () {
    const { error } = this.state
    const { children } = this.props

    if (error) {
      const title = 'Произошла ошибка'
      const message = error.message

      return (
        <div className="ErrorBoundary">
          <Alert
            type="error"
            showIcon={true}
            message={title}
            description={message}
          />
        </div>
      )
    }

    return children
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired
}

export default ErrorBoundary
