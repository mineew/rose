import { call, takeEvery, all } from 'redux-saga/effects'
import { message, Modal } from 'antd'
import * as t from '../actions/types'

export const loadingMessage = 'Выполняется операция...'
export const loadingMessageDuration = 0
export const errorTitle = 'Произошла ошибка'

export function * loadingNotification () {
  yield call(message.loading, loadingMessage, loadingMessageDuration)
}

export function * errorNotification ({ payload }) {
  yield call(Modal.error, {
    errorTitle,
    content: payload.message
  })
}

export function * destroyNotifications () {
  yield call(message.destroy)
}

export default function * () {
  yield all([
    takeEvery([
      t.FILE_OPEN_START,
      t.FILE_SAVE_START,
      t.FILE_SAVE_AS_START,
      t.IMPORT_START
    ], loadingNotification),

    takeEvery([
      t.FILE_OPEN_ERROR,
      t.FILE_SAVE_ERROR,
      t.FILE_SAVE_AS_ERROR,
      t.IMPORT_ERROR
    ], errorNotification),

    takeEvery([
      t.FILE_OPEN_ERROR,
      t.FILE_SAVE_ERROR,
      t.FILE_SAVE_AS_ERROR,
      t.IMPORT_ERROR,
      t.FILE_OPEN_SUCCESS,
      t.FILE_SAVE_AS_SUCCESS,
      t.FILE_SAVE_SUCCESS,
      t.IMPORT_SUCCESS,
      t.FILE_CLOSE
    ], destroyNotifications)
  ])
}
