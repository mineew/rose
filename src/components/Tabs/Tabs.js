import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Tabs as AntdTabs, Icon } from 'antd'

/**
 * @typedef {Object} TabOptions
 * @property {string} title
 * @property {string} icon
 * @property {React.ReactNode} content
 */

/**
 * @typedef {Object} TabsProps
 * @property {Array<TabOptions>} tabs
 */

/**
 * @param {TabsProps} props
 * @returns {React.Component<TabsProps>}
 */
function Tabs (props) {
  const { tabs } = props

  const [selectedTab, setSelectedTab] = useState(tabs[0].title)

  const handleChange = (title) => {
    setSelectedTab(title)
  }

  const renderTabTitle = (icon, title) => (
    <span>
      <Icon type={icon} /> {title}
    </span>
  )

  return (
    <AntdTabs
      activeKey={selectedTab}
      onChange={handleChange}
    >
      {tabs.map(({ title, icon, content }) => (
        <AntdTabs.TabPane
          key={title}
          tab={renderTabTitle(icon, title)}
        >
          {content}
        </AntdTabs.TabPane>
      ))}
    </AntdTabs>
  )
}

const tabType = PropTypes.shape({
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  content: PropTypes.node.isRequired
})

Tabs.propTypes = {
  tabs: PropTypes.arrayOf(tabType).isRequired
}

export default Tabs
