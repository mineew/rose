const HtmlWebpackPlugin = require('html-webpack-plugin')

/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @param {Configuration} base
 * @returns {Configuration}
 */
module.exports = (base) => {
  const isProduction = base.mode === 'production'

  const htmlPlugin = new HtmlWebpackPlugin({
    template: './src/index.html',
    minify: isProduction && {
      removeComments: true,
      collapseWhitespace: true,
      removeRedundantAttributes: true,
      useShortDoctype: true,
      removeEmptyAttributes: true,
      removeStyleLinkTypeAttributes: true,
      keepClosingSlash: true,
      minifyJS: true,
      minifyCSS: true,
      minifyURLs: true
    }
  })

  return {
    plugins: [htmlPlugin]
  }
}
