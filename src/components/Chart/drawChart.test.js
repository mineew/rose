import Highcharts from 'highcharts'
import drawChart, {
  angleIsValid, valueIsValid, prepareChartData
} from './drawChart'

Highcharts.chart = jest.fn()

/** @type {import('./Chart').ChartProps} */
const props = {
  data: [[15, 100], [30, 120], [330, 105], [185, 110]],
  title: 'Title',
  subtitle: 'Subtitle',
  tickInterval: 5,
  step: 3,
  labelsSize: 11,
  labelsColor: '#666666',
  gridColor: '#f5f5f5',
  showLines: true,
  linesColor: '#ff0000',
  linesWidth: 1,
  showAreas: true,
  areasBgColor: '#ff0000',
  areasOpacity: 50,
  areasLineColor: '#ff0000',
  areasLineWidth: 1
}

/**
 * @returns {import('highcharts').Options}
 */
function getLastOptions () {
  const { calls } = Highcharts.chart.mock
  return calls[calls.length - 1][1]
}

describe('drawChart', () => {
  it('configures the titles', () => {
    drawChart(null, props)
    const options = getLastOptions()
    expect(options.title).toEqual({ text: props.title })
    expect(options.subtitle).toEqual({ text: props.subtitle })
  })

  it('configures the x-axis', () => {
    drawChart(null, props)
    const { xAxis } = getLastOptions()
    expect(xAxis.tickInterval).toBe(props.tickInterval)
    expect(xAxis.labels.step).toBe(props.step)
    expect(xAxis.labels.style.fontSize).toBe(`${props.labelsSize}px`)
    expect(xAxis.labels.style.color).toBe(props.labelsColor)
    expect(xAxis.lineColor).toBe(props.gridColor)
    expect(xAxis.gridLineColor).toBe(props.gridColor)
    xAxis.labels.value = 95
    expect(xAxis.labels.formatter()).toBe('5°')
    xAxis.labels.value = 15
    expect(xAxis.labels.formatter()).toBe('285°')
  })

  it('configures the y-axis', () => {
    drawChart(null, props)
    const { yAxis } = getLastOptions()
    expect(yAxis.labels.style.fontSize).toBe(`${props.labelsSize}px`)
    expect(yAxis.labels.style.color).toBe(props.labelsColor)
    expect(yAxis.lineColor).toBe(props.gridColor)
    expect(yAxis.gridLineColor).toBe(props.gridColor)
    yAxis.labels.value = 120
    expect(yAxis.labels.formatter()).toBe(120)
  })

  it('configures the plot options', () => {
    drawChart(null, props)
    const { plotOptions } = getLastOptions()
    expect(plotOptions.series.pointInterval).toBe(props.tickInterval)
    expect(plotOptions.line.color).toBe(props.linesColor)
    expect(plotOptions.line.lineWidth).toBe(props.linesWidth)
    expect(plotOptions.area.color).toBe(props.areasBgColor)
    expect(plotOptions.area.fillOpacity).toBeCloseTo(props.areasOpacity / 100)
    expect(plotOptions.area.lineColor).toBe(props.areasLineColor)
    expect(plotOptions.area.lineWidth).toBe(props.areasLineWidth)
  })

  it('configures the series', () => {
    drawChart(null, props)
    const { series } = getLastOptions()
    expect(series[0].type).toBe('line')
    expect(series[0].data).toEqual([[0, 0], [15, 100]])
    expect(series[1].type).toBe('line')
    expect(series[1].data).toEqual([[0, 0], [30, 120]])
    expect(series[2].type).toBe('line')
    expect(series[2].data).toEqual([[0, 0], [330, 105]])
    expect(series[3].type).toBe('line')
    expect(series[3].data).toEqual([[0, 0], [185, 110]])
    expect(series[4].type).toBe('area')
    expect(series[4].data).toEqual([
      [0, 0],
      [15, 100],
      [30, 120],
      [330, 105],
      [185, 110]
    ])
  })

  it('can disable lines', () => {
    drawChart(null, { ...props, showLines: false })
    const { series } = getLastOptions()
    series.map(({ type }) => {
      expect(type).not.toMatch('line')
    })
  })

  it('can disable areas', () => {
    drawChart(null, { ...props, showAreas: false })
    const { series } = getLastOptions()
    series.map(({ type }) => {
      expect(type).not.toMatch('area')
    })
  })

  it('can draw empty chart', () => {
    drawChart(null, { ...props, data: [] })
    const { series } = getLastOptions()
    expect(series).toHaveLength(1)
    expect(series[0].data).toBeUndefined()
  })
})

describe('angleIsValid', () => {
  it('validates an angle', () => {
    expect(angleIsValid('0')).toBeTruthy()
    expect(angleIsValid('45')).toBeTruthy()
    expect(angleIsValid('90')).toBeTruthy()
    expect(angleIsValid('180')).toBeTruthy()
    expect(angleIsValid('270')).toBeTruthy()
    expect(angleIsValid('359')).toBeTruthy()
    expect(angleIsValid('360')).toBeFalsy()
    expect(angleIsValid('370')).toBeFalsy()
    expect(angleIsValid(' -1')).toBeFalsy()
  })
})

describe('valueIsValid', () => {
  it('validates a value', () => {
    expect(valueIsValid('123')).toBeTruthy()
    expect(valueIsValid('123.456')).toBeTruthy()
    expect(valueIsValid('')).toBeFalsy()
    expect(valueIsValid('asdf')).toBeFalsy()
    expect(valueIsValid('  ')).toBeFalsy()
  })
})

describe('prepareChartData', () => {
  it('filters and transforms data for the chart', () => {
    expect(prepareChartData([
      ['15', '100'],
      ['15', '100asd'],
      ['30', '120'],
      ['45', '90'],
      ['60', '110'],
      ['60', ''],
      ['345', '85'],
      ['330', '105'],
      ['330,122', '105'],
      ['315', '70'],
      ['185', '110']
    ])).toEqual([
      [45, 70],
      [60, 105],
      [75, 85],
      [105, 100],
      [120, 120],
      [135, 90],
      [150, 110]
    ])
  })

  it('collapses mirrored angles', () => {
    expect(prepareChartData([
      ['15', '100'],
      ['30', '120'],
      ['210', '30']
    ])).toEqual([
      [105, 100],
      [120, 150]
    ])
  })
})
