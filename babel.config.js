module.exports = (api) => {
  const isProduction = api.env('production')

  api.cache.using(() => isProduction)

  const presets = [
    ['@babel/env', {
      useBuiltIns: 'usage',
      corejs: 3
    }],
    ['@babel/react', {
      useBuiltIns: false,
      development: !isProduction
    }]
  ]

  const plugins = [
    '@babel/proposal-class-properties',
    '@babel/syntax-dynamic-import'
  ]

  return {
    presets,
    plugins
  }
}
