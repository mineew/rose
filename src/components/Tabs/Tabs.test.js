import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import Tabs from './Tabs'

describe('<Tabs />', () => {
  const tabs = [{
    title: 'Data Tab Title',
    icon: 'database',
    content: 'Data Tab Content'
  }, {
    title: 'SettingsTabTitle',
    icon: 'setting',
    content: 'Settings Tab Content'
  }]

  const wrapper = () => render(
    <Tabs tabs={tabs} />
  )

  it('renders tabs', () => {
    const { baseElement } = wrapper()
    for (let { title } of tabs) {
      expect(baseElement.innerHTML).toMatch(title)
    }
  })

  it('sets first tab as active', () => {
    const { getByText } = wrapper()
    const { parentElement: firstTab } = getByText(tabs[0].title)
    expect(firstTab.className).toMatch('active')
  })

  it('can change active tab', () => {
    const { getByText } = wrapper()
    const { parentElement: secondTab } = getByText(tabs[1].title)
    expect(secondTab.className).not.toMatch('active')
    fireEvent.click(secondTab)
    expect(secondTab.className).toMatch('active')
  })
})
