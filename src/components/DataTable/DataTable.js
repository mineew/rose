import React from 'react'
import PropTypes from 'prop-types'
import { Button, Empty } from 'antd'
import DataPointInput from '../DataPointInput/DataPointInput'
import './DataTable.css'

/**
 * @typedef {Object} DataTableProps
 * @property {Array<[string, string]>} value
 * @property {function(Array<[string, string]>)} onChange
 * @property {number} [addRowsCount]
 */

/**
 * @param {DataTableProps} props
 * @returns {React.Component<DataTableProps>}
 */
function DataTable (props) {
  const { value: dataPoints, onChange, addRowsCount } = props

  const handleDataPointChange = (index, changedPoint) => {
    onChange(dataPoints.map((point, i) => {
      return i === index
        ? changedPoint
        : point
    }))
  }

  const handleDeleteDataPointClick = (index) => {
    onChange(dataPoints.filter(
      (_, i) => i !== index)
    )
  }

  const handleAddDataPointsClick = () => {
    onChange(dataPoints.concat(
      Array(addRowsCount).fill(['', ''])
    ))
  }

  return (
    <div className="DataTable">
      {!dataPoints.length ? (
        <Empty
          description="Нет данных"
          image={Empty.PRESENTED_IMAGE_SIMPLE}
        />
      ) : (
        renderDataPoints(
          dataPoints,
          handleDataPointChange,
          handleDeleteDataPointClick
        )
      )}
      <Button
        type="primary"
        icon="plus"
        shape="circle"
        size="large"
        onClick={handleAddDataPointsClick}
      />
    </div>
  )
}

DataTable.defaultProps = {
  addRowsCount: 5
}

DataTable.propTypes = {
  value: PropTypes.arrayOf(
    PropTypes.arrayOf(PropTypes.string)
  ).isRequired,
  onChange: PropTypes.func.isRequired,
  addRowsCount: PropTypes.number
}

export default DataTable

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {Array<[string, string]>} dataPoints
 * @param {function(number, [string, string])} onChange
 * @param {function(number)} onDelete
 */
function renderDataPoints (dataPoints, onChange, onDelete) {
  return dataPoints.map((point, i) => (
    <div key={i}>
      <DataPointInput
        value={point}
        onChange={(p) => onChange(i, p)}
      />
      <Button
        type="dashed"
        icon="delete"
        shape="circle"
        onClick={() => onDelete(i)}
      />
    </div>
  ))
}
