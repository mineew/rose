import React from 'react'
import { render } from 'react-testing-library'
import CollapseCard from './CollapseCard'

describe('<CollapseCard />', () => {
  const title = 'Collapse Card Title'
  const children = 'Collapse Card Content'
  const icon = 'user'

  const wrapper = (props = {}) => render(
    <CollapseCard title={title} icon={icon} {...props}>
      {children}
    </CollapseCard>
  )

  it('renders a title', () => {
    const { baseElement } = wrapper()
    expect(baseElement.innerHTML).toMatch(title)
  })

  it('renders children', () => {
    const { baseElement } = wrapper()
    expect(baseElement.innerHTML).toMatch(children)
  })

  it('renders icon', () => {
    const { baseElement } = wrapper()
    expect(baseElement.innerHTML).toMatch(icon)
  })

  it('does not render icon', () => {
    const { baseElement } = wrapper({ icon: undefined })
    expect(baseElement.innerHTML).not.toMatch(icon)
  })
})
