import { put, call, select, takeLatest, all } from 'redux-saga/effects'
import * as t from '../actions/types'
import * as a from '../actions/creators'
import { appData, manage } from '../store/selectors'
import * as electron from './electron'

export const jsonFileFilter = {
  name: 'Файлы JSON',
  extensions: ['json']
}

export const excelFileFilter = {
  name: 'Файлы Excel',
  extensions: ['xlsx', 'xls']
}

export function * openFile () {
  try {
    const path = yield call(electron.getFileOpenPath, jsonFileFilter)
    if (path) {
      yield put(a.fileOpenStart())
      const data = yield call(electron.readFile, path)
      yield put(a.fileOpenSuccess(
        path,
        electron.getFileName(path),
        electron.deserializeData(data)
      ))
    }
  } catch (error) {
    yield put(a.fileOpenError(error))
  }
}

export function * saveAsFile () {
  try {
    const path = yield call(electron.getFileSavePath, jsonFileFilter)
    if (path) {
      yield put(a.fileSaveAsStart())
      const state = yield select()
      const data = appData(state)
      yield call(electron.saveFile, path, electron.serializeData(data))
      yield put(a.fileSaveAsSuccess(
        path,
        electron.getFileName(path),
        data
      ))
    }
  } catch (error) {
    yield put(a.fileSaveAsError(error))
  }
}

export function * saveFile () {
  try {
    const state = yield select()
    const path = manage(state).filePath
    const data = appData(state)
    yield call(electron.saveFile, path, electron.serializeData(data))
    yield put(a.fileSaveSuccess(data))
  } catch (error) {
    yield put(a.fileSaveError(error))
  }
}

export function * importChartData () {
  try {
    const path = yield call(electron.getFileOpenPath, excelFileFilter)
    if (path) {
      yield put(a.importStart())
      const chartData = yield call(electron.importFromExcel, path)
      yield put(a.importSuccess(chartData))
    }
  } catch (error) {
    yield put(a.importError(error))
  }
}

export function * quit () {
  yield call(electron.quit)
}

export default function * () {
  yield all([
    takeLatest(t.FILE_OPEN_REQUEST, openFile),
    takeLatest(t.FILE_SAVE_AS_REQUEST, saveAsFile),
    takeLatest(t.FILE_SAVE_START, saveFile),
    takeLatest(t.IMPORT_REQUEST, importChartData),
    takeLatest(t.QUIT, quit)
  ])
}
