/** @typedef {import('./FileDropdownMenu').FileDropdownMenuProps} FileDropdownMenuProps */

import React from 'react'
import { Modal } from 'antd'
import { render, fireEvent } from 'react-testing-library'
import { FileDropdownMenu } from './FileDropdownMenu'

Modal.confirm = jest.fn()

/** @type {FileDropdownMenuProps} */
const commonProps = {
  fileIsOpened: false,
  needSave: false,
  onFileCloseClick: jest.fn(),
  onFileOpenClick: jest.fn(),
  onFileSaveClick: jest.fn(),
  onImportClick: jest.fn(),
  onQuitClick: jest.fn(),
  onSaveAsClick: jest.fn()
}

afterEach(() => {
  commonProps.onFileCloseClick.mockClear()
  commonProps.onFileOpenClick.mockClear()
  commonProps.onFileSaveClick.mockClear()
  commonProps.onImportClick.mockClear()
  commonProps.onQuitClick.mockClear()
  commonProps.onSaveAsClick.mockClear()
  Modal.confirm.mockClear()
})

/** @param {FileDropdownMenuProps} testProps */
const wrapper = (testProps = {}) => {
  const props = { ...commonProps, ...testProps }

  return render(
    <FileDropdownMenu {...props} />
  )
}

/**
 * @param {HTMLElement} container
 * @param {string} icon
 */
function clickOnMenuItemByIcon (container, icon) {
  const menuItem = container.querySelector(`.anticon-${icon}`).parentElement
  fireEvent.click(menuItem)
}

describe('<FileMenu /> - dropdown menu', () => {
  it('opens files', () => {
    const { container } = wrapper()
    clickOnMenuItemByIcon(container, 'folder-open')
    expect(commonProps.onFileOpenClick).toHaveBeenCalledTimes(1)
  })

  it('asks for confirmation when opening a file', () => {
    const { container } = wrapper({ needSave: true })
    clickOnMenuItemByIcon(container, 'folder-open')
    expect(Modal.confirm).toHaveBeenCalledTimes(1)
  })

  it('closes files', () => {
    const { container } = wrapper({ fileIsOpened: true })
    clickOnMenuItemByIcon(container, 'close')
    expect(commonProps.onFileCloseClick).toHaveBeenCalledTimes(1)
  })

  it('asks for confirmation when closing a file', () => {
    const { container } = wrapper({ fileIsOpened: true, needSave: true })
    clickOnMenuItemByIcon(container, 'close')
    expect(Modal.confirm).toHaveBeenCalledTimes(1)
  })

  it('saves files', () => {
    const { container } = wrapper({ fileIsOpened: true, needSave: true })
    clickOnMenuItemByIcon(container, 'file-sync')
    expect(commonProps.onFileSaveClick).toHaveBeenCalledTimes(1)
  })

  it('saves copies of files', () => {
    const { container } = wrapper({ needSave: true })
    clickOnMenuItemByIcon(container, 'save')
    expect(commonProps.onSaveAsClick).toHaveBeenCalledTimes(1)
  })

  it('imports files', () => {
    const { container } = wrapper()
    clickOnMenuItemByIcon(container, 'file-excel')
    expect(commonProps.onImportClick).toHaveBeenCalledTimes(1)
  })

  it('asks for confirmation when importing a file', () => {
    const { container } = wrapper({ needSave: true })
    clickOnMenuItemByIcon(container, 'file-excel')
    expect(Modal.confirm).toHaveBeenCalledTimes(1)
  })

  it('quits the app', () => {
    const { container } = wrapper()
    clickOnMenuItemByIcon(container, 'logout')
    expect(commonProps.onQuitClick).toHaveBeenCalledTimes(1)
  })

  it('asks for confirmation when quiting', () => {
    const { container } = wrapper({ needSave: true })
    clickOnMenuItemByIcon(container, 'logout')
    expect(Modal.confirm).toHaveBeenCalledTimes(1)
  })
})
