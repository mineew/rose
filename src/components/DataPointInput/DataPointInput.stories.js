import React from 'react'
import { storiesOf } from '@storybook/react'
import { object } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'
import DataPointInput from './DataPointInput'

const stories = storiesOf('Components', module)

stories.add('DataPointInput', () => {
  const valueKnob = object('value', ['129', '150'])

  return (
    <div style={{ margin: 20 }}>
      <DataPointInput
        value={valueKnob}
        onChange={action('onChange')}
      />
    </div>
  )
})
