import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import CheckInput from './CheckInput'

describe('<CheckInput />', () => {
  const value = false
  const onChange = jest.fn()
  const label = 'Check Input Label'

  const wrapper = (props = {}) => render(
    <CheckInput
      value={value}
      onChange={onChange}
      label={label}
      {...props}
    />
  )

  it('changes the value', () => {
    const { getByText } = wrapper()
    const { parentElement } = getByText(label)
    fireEvent.click(parentElement.firstChild)
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(!value)
  })

  it('takes the `label` prop', () => {
    const { baseElement } = wrapper()
    expect(baseElement.innerHTML).toMatch(label)
  })
})
