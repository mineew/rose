import { connect } from 'react-redux'
import { settings } from '../../store/selectors'

/**
 * @typedef {import('react').Component} Component
 * @typedef {import('../../actions').Action} Action
 */

/**
 * @param {Component} InputComponent
 * @param {function(object): any} getValueProp
 * @param {function(any): Action} changeValueActionCreator
 * @param {object} defaultProps
 * @returns {Component}
 */
function connectSettingsInput (
  InputComponent,
  getValueProp,
  changeValueActionCreator,
  defaultProps
) {
  const mapStateToProps = (state) => ({
    value: getValueProp(settings(state))
  })

  const mapDispatchToProps = (dispatch) => ({
    onChange (value) {
      dispatch(changeValueActionCreator(value))
    }
  })

  const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...ownProps,
    ...defaultProps,
    ...stateProps,
    ...dispatchProps
  })

  return connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(InputComponent)
}

export default connectSettingsInput
