import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import TextInput from './TextInput'

describe('<TextInput />', () => {
  const value = '#ffffff'
  const changedValue = '#000000'
  const onChange = jest.fn()
  const label = 'some label'
  const type = 'color'

  const wrapper = () => render(
    <TextInput
      type={type}
      value={value}
      onChange={onChange}
      label={label}
    />
  )

  it('changes the value', () => {
    const { getByDisplayValue } = wrapper()
    fireEvent.change(getByDisplayValue(value), {
      target: { value: changedValue }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(changedValue)
  })

  it('takes the `type` prop', () => {
    const { getByDisplayValue } = wrapper()
    expect(getByDisplayValue(value).type).toBe(type)
  })

  it('takes the `label` prop', () => {
    const { baseElement } = wrapper()
    expect(baseElement.innerHTML).toMatch(label)
  })
})
