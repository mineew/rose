import { NumberInput } from '../../components'
import { setLinesWidth } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  NumberInput,
  (state) => state.linesWidth,
  setLinesWidth,
  {
    label: 'Толщина линий',
    min: 0,
    max: 10
  }
)
