import { TextInput } from '../../components'
import { setTitle } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  TextInput,
  (state) => state.title,
  setTitle,
  {
    label: 'Заголовок'
  }
)
