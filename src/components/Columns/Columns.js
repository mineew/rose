import React, { Children as ReactChildrenUtils } from 'react'
import PropTypes from 'prop-types'
import { Layout } from 'antd'
import './Columns.css'

const { Sider, Content } = Layout
const { toArray } = ReactChildrenUtils

/**
 * @typedef {Object} ColumnsProps
 * @property {React.ReactNode} children
 * @property {number} [leftColumnWidth]
 */

/**
 * @param {ColumnsProps} props
 * @returns {React.Component<ColumnsProps>}
 */
function Columns (props) {
  const { children, leftColumnWidth } = props

  const [left, right] = toArray(children)

  return (
    <Layout className="Columns">
      <Sider
        className="Columns__left"
        width={leftColumnWidth}
      >
        {left}
      </Sider>
      <Content
        className="Columns__right"
      >
        {right}
      </Content>
    </Layout>
  )
}

Columns.defaultProps = {
  leftColumnWidth: 310
}

Columns.propTypes = {
  children: PropTypes.node.isRequired,
  leftColumnWidth: PropTypes.number
}

export default Columns
