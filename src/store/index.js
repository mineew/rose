import { createStore, applyMiddleware, compose as reduxCompose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger'
import rootReducer from '../reducers'
import rootSaga from '../sagas'

/// middlewares ----------------------------------------------------------------

const middlewares = []

if (process.env.NODE_ENV === 'development') {
  middlewares.push(logger)
}

const sagaMiddleware = createSagaMiddleware()
middlewares.push(sagaMiddleware)

/// enhancers ------------------------------------------------------------------

const enhancers = []

if (middlewares.length) {
  enhancers.push(applyMiddleware(...middlewares))
}

const compose = process.env.NODE_ENV === 'production'
  ? reduxCompose
  : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : reduxCompose

/// create store ---------------------------------------------------------------

/** @type {import('redux').Store} */
const store = createStore(
  rootReducer,
  // preloadedState,
  compose(...enhancers)
)

sagaMiddleware.run(rootSaga)

export default store
