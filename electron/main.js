/* eslint-disable no-console */

const pt = require('path')
const { app, BrowserWindow } = require('electron')
const isDev = require('electron-is-dev')
const { productName, version } = require('../package.json')

const REACT_DEV_URL = 'http://localhost:3000'
const REACT_PROD_FILE = '../build/index.html'

/** @type {BrowserWindow} */
let mainWindow

function createMainWindow () {
  mainWindow = new BrowserWindow({
    title: `${productName} - v${version}`,
    width: 900,
    height: 700,
    webPreferences: {
      preload: pt.join(__dirname, 'preload.js')
    }
  })
  if (isDev) {
    mainWindow.loadURL(REACT_DEV_URL)
    mainWindow.webContents.openDevTools({
      mode: 'undocked'
    })
  } else {
    mainWindow.loadFile(pt.resolve(__dirname, REACT_PROD_FILE))
    mainWindow.setMenuBarVisibility(false)
  }
  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', () => {
  createMainWindow()

  if (isDev) {
    const {
      default: installExtension,
      REACT_DEVELOPER_TOOLS,
      REDUX_DEVTOOLS
    } = require('electron-devtools-installer')

    const installed = Object.keys(
      BrowserWindow.getDevToolsExtensions()
    )

    if (!installed.includes('React Developer Tools')) {
      installExtension(REACT_DEVELOPER_TOOLS)
        .catch((error) => console.error(error))
    }

    if (!installed.includes('Redux DevTools')) {
      installExtension(REDUX_DEVTOOLS)
        .catch((error) => console.error(error))
    }
  }
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createMainWindow()
  }
})
