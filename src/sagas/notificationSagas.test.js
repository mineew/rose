import { call, takeEvery, all } from 'redux-saga/effects'
import { message, Modal } from 'antd'
import * as t from '../actions/types'
import notificationSagas, {
  loadingNotification,
  errorNotification,
  destroyNotifications,
  loadingMessage,
  loadingMessageDuration,
  errorTitle
} from './notificationSagas'

describe('notification sagas', () => {
  describe('loadingNotification', () => {
    it('shows loading message', () => {
      const generator = loadingNotification()
      expect(generator.next().value).toEqual(
        call(message.loading, loadingMessage, loadingMessageDuration)
      )
    })
  })

  describe('errorNotification', () => {
    it('shows error message', () => {
      const error = new Error('some error message')
      const generator = errorNotification({ payload: error })
      expect(generator.next().value).toEqual(
        call(Modal.error, {
          errorTitle,
          content: error.message
        })
      )
    })
  })

  describe('destroyNotifications', () => {
    it('closes all notifications', () => {
      const generator = destroyNotifications()
      expect(generator.next().value).toEqual(
        call(message.destroy)
      )
    })
  })

  describe('notificationSagas', () => {
    it('runs notification sagas', () => {
      const generator = notificationSagas()
      expect(generator.next().value).toEqual(
        all([
          takeEvery([
            t.FILE_OPEN_START,
            t.FILE_SAVE_START,
            t.FILE_SAVE_AS_START,
            t.IMPORT_START
          ], loadingNotification),

          takeEvery([
            t.FILE_OPEN_ERROR,
            t.FILE_SAVE_ERROR,
            t.FILE_SAVE_AS_ERROR,
            t.IMPORT_ERROR
          ], errorNotification),

          takeEvery([
            t.FILE_OPEN_ERROR,
            t.FILE_SAVE_ERROR,
            t.FILE_SAVE_AS_ERROR,
            t.IMPORT_ERROR,
            t.FILE_OPEN_SUCCESS,
            t.FILE_SAVE_AS_SUCCESS,
            t.FILE_SAVE_SUCCESS,
            t.IMPORT_SUCCESS,
            t.FILE_CLOSE
          ], destroyNotifications)
        ])
      )
    })
  })
})
