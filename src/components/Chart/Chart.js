import React, { useRef, useEffect } from 'react'
import PropTypes from 'prop-types'
import drawChart from './drawChart'
import './Chart.css'

/**
 * @typedef {Object} ChartProps
 * @property {Array<[number, number]>} data
 * @property {string} title
 * @property {string} subtitle
 * @property {number} tickInterval
 * @property {number} step
 * @property {number} labelsSize
 * @property {string} labelsColor
 * @property {string} gridColor
 * @property {boolean} showLines
 * @property {string} linesColor
 * @property {number} linesWidth
 * @property {boolean} showAreas
 * @property {string} areasBgColor
 * @property {number} areasOpacity
 * @property {string} areasLineColor
 * @property {number} areasLineWidth
 */

/**
 * @param {ChartProps} props
 * @returns {React.Component<ChartProps>}
 */
function Chart (props) {
  /** @type {{current: HTMLDivElement}} */
  const chart = useRef(null)

  useEffect(() => {
    drawChart(chart.current, props)
  })

  return (
    <div className="Chart" ref={chart} />
  )
}

Chart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)).isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  tickInterval: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
  labelsSize: PropTypes.number.isRequired,
  labelsColor: PropTypes.string.isRequired,
  gridColor: PropTypes.string.isRequired,
  showLines: PropTypes.bool,
  linesColor: PropTypes.string.isRequired,
  linesWidth: PropTypes.number.isRequired,
  showAreas: PropTypes.bool,
  areasBgColor: PropTypes.string.isRequired,
  areasOpacity: PropTypes.number.isRequired,
  areasLineColor: PropTypes.string.isRequired,
  areasLineWidth: PropTypes.number.isRequired
}

export default Chart
