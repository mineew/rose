import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Dropdown, Button, Icon, Badge } from 'antd'
import { FileDropdownMenu as renderDropdownMenu } from './FileDropdownMenu'
import './FileMenu.css'

/**
 * @typedef {Object} FileMenuProps
 * @property {string} [fileName]
 * @property {boolean} [needSave]
 * @property {function} [onFileOpenClick]
 * @property {function} [onFileCloseClick]
 * @property {function} [onFileSaveClick]
 * @property {function} [onSaveAsClick]
 * @property {function} [onImportClick]
 * @property {function} [onQuitClick]
 */

/**
 * @param {FileMenuProps} props
 * @returns {React.Component<FileMenuProps>}
 */
function FileMenu (props) {
  const {
    fileName,
    needSave,
    onFileOpenClick,
    onFileCloseClick,
    onFileSaveClick,
    onSaveAsClick,
    onImportClick,
    onQuitClick
  } = props

  const fileIsOpened = Boolean(fileName)

  const menu = renderDropdownMenu({
    fileIsOpened,
    needSave,
    onFileOpenClick,
    onFileCloseClick,
    onFileSaveClick,
    onSaveAsClick,
    onImportClick,
    onQuitClick
  })

  return (
    <Fragment>
      <Dropdown trigger={['click']} overlay={menu}>
        <Badge dot={true} count={Number(needSave)}>
          <Button icon="file">
            Файл <Icon type="down" />
          </Button>
        </Badge>
      </Dropdown>
      {fileIsOpened && (
        <Button
          className="FileMenu__filename"
          icon="folder-open"
          disabled={true}
          ghost={true}
        >
          {fileName}
        </Button>
      )}
    </Fragment>
  )
}

FileMenu.defaultProps = {
  fileName: '',
  needSave: false,
  onFileOpenClick: () => {},
  onFileCloseClick: () => {},
  onFileSaveClick: () => {},
  onSaveAsClick: () => {},
  onImportClick: () => {},
  onQuitClick: () => {}
}

FileMenu.propTypes = {
  fileName: PropTypes.string,
  needSave: PropTypes.bool,
  onFileOpenClick: PropTypes.func,
  onFileCloseClick: PropTypes.func,
  onFileSaveClick: PropTypes.func,
  onSaveAsClick: PropTypes.func,
  onImportClick: PropTypes.func,
  onQuitClick: PropTypes.func
}

export default FileMenu
