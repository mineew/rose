import React from 'react'
import { render } from 'react-testing-library'
import * as drawChart from './drawChart'
import Chart from './Chart'

drawChart.default = jest.fn()

/** @type {import('./Chart').ChartProps} */
const props = {
  data: [[15, 100], [30, 120], [330, 105], [185, 110]],
  title: 'Title',
  subtitle: 'Subtitle',
  tickInterval: 5,
  step: 3,
  labelsSize: 11,
  labelsColor: '#666666',
  gridColor: '#f5f5f5',
  showLines: true,
  linesColor: '#ff0000',
  linesWidth: 1,
  showAreas: true,
  areasBgColor: '#ff0000',
  areasOpacity: 50,
  areasLineColor: '#ff0000',
  areasLineWidth: 1
}

describe('<Chart /> component', () => {
  it('draws a chart', () => {
    render(<Chart {...props} />)
    expect(drawChart.default).toHaveBeenCalledTimes(1)
  })
})
