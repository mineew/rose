import { setDataPoints } from '../../actions'
import { mapStateToProps, mapDispatchToProps } from './DataTable'

describe('<DataTable /> container', () => {
  it('maps chart data from state to `value` prop', () => {
    const state = {
      appData: {
        chartData: [
          ['15', '100']
        ]
      }
    }
    const props = {
      value: [
        ['15', '100']
      ]
    }
    expect(mapStateToProps(state)).toEqual(props)
  })

  it('maps `setDataPoints` action to `onChange` prop', () => {
    const dispatch = jest.fn()
    const props = mapDispatchToProps(dispatch)
    props.onChange([['15', '100']])
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch).toHaveBeenLastCalledWith(
      setDataPoints([['15', '100']])
    )
  })
})
