import * as reactRedux from 'react-redux'
import connectSettingsInput from './connectSettingsInput'

reactRedux.connect = jest.fn(() => jest.fn())

describe('connectSettingsInput', () => {
  connectSettingsInput(
    null,
    (state) => state.someStateValue,
    (value) => ({ type: 'SOME_ACTION', payload: value }),
    { someDefaultProp: 'value' }
  )

  const [
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ] = reactRedux.connect.mock.calls[0]

  it('maps `value` prop', () => {
    const props = mapStateToProps({
      appData: {
        settings: {
          someStateValue: 'someStateValue'
        }
      }
    })
    expect(props).toEqual({ value: 'someStateValue' })
  })

  it('maps `onChange` prop', () => {
    const dispatch = jest.fn()
    const props = mapDispatchToProps(dispatch)
    props.onChange('some new value')
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch).toHaveBeenLastCalledWith({
      type: 'SOME_ACTION',
      payload: 'some new value'
    })
  })

  it('can add default props', () => {
    const props = mergeProps({}, {}, {})
    expect(props.someDefaultProp).toBe('value')
  })
})
