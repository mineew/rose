import { createSelector } from 'reselect'
import { prepareChartData } from '../components'

export const appData = (state) => state.appData

export const manage = (state) => state.manage

export const settings = createSelector(
  appData,
  (appData) => appData.settings
)

export const rawChartData = createSelector(
  appData,
  (appData) => appData.chartData
)

export const preparedChartData = createSelector(
  appData,
  (appData) => prepareChartData(appData.chartData)
)

export const haveUnsavedChanges = createSelector(
  [manage, appData],
  (manage, appData) => {
    return JSON.stringify(manage.lastSavedAppData) !== JSON.stringify(appData)
  }
)
