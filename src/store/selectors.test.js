import * as selectors from './selectors'
import { prepareChartData } from '../components'

const state = {
  appData: {
    chartData: [['0', '100'], ['180', '20'], ['15', '45']],
    settings: {}
  },
  manage: {
    lastSavedAppData: null
  }
}

describe('selectors', () => {
  describe('appData', () => {
    it('selects `appData`', () => {
      expect(selectors.appData(state)).toEqual(state.appData)
    })
  })

  describe('manage', () => {
    it('selects `manage`', () => {
      expect(selectors.manage(state)).toEqual(state.manage)
    })
  })

  describe('settings', () => {
    it('selects `appData.settings`', () => {
      expect(selectors.settings(state)).toEqual(state.appData.settings)
    })
  })

  describe('rawChartData', () => {
    it('selects `appData.chartData`', () => {
      expect(selectors.rawChartData(state)).toEqual(state.appData.chartData)
    })
  })

  describe('preparedChartData', () => {
    it('selects `appData.chartData` and prepares it for the chart', () => {
      expect(selectors.preparedChartData(state)).toEqual(
        prepareChartData(state.appData.chartData)
      )
    })
  })

  describe('haveUnsavedChanges', () => {
    it('compares last saved app data and current app data', () => {
      expect(selectors.haveUnsavedChanges(state)).toBeTruthy()
    })
  })
})
