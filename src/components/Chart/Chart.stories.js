/* eslint-disable react/prop-types */

import React, { Fragment, useState } from 'react'
import { storiesOf } from '@storybook/react'
import { text, number, color, boolean } from '@storybook/addon-knobs'
import { prepareChartData } from './drawChart'
import Chart from './Chart'

const exampleData = prepareChartData([
  ['15', '100'],
  ['30', '120'],
  ['45', '90'],
  ['60', '110'],
  ['345', '85'],
  ['330', '105'],
  ['315', '70']
])

function Example (props) {
  const [ withData, setWithData ] = useState(true)

  return (
    <Fragment>
      <label>
        <input
          type="checkbox"
          checked={withData}
          onChange={() => setWithData(!withData)}
        />
        Draw this <code>Chart</code> component with the following data:
      </label>
      <pre>{JSON.stringify(exampleData)}</pre>
      <Chart
        data={withData ? exampleData : []}
        {...props}
      />
    </Fragment>
  )
}

const stories = storiesOf('Components', module)

stories.add('Chart', () => {
  const titleKnob = text('title', '')
  const subtitleKnob = text('subtitle', '')
  const tickIntervalKnob = number('tickInterval', 5, {
    range: true, min: 1, max: 20
  })
  const stepKnob = number('step', 3, {
    range: true, min: 1, max: 20
  })
  const labelsSizeKnob = number('labelsSize', 11, {
    range: true, min: 1, max: 20
  })
  const labelsColorKnob = color('labelsColor', '#666666')
  const gridColorKnob = color('gridColor', '#e6e6e6')
  const showLinesKnob = boolean('showLines', true)
  const linesColorKnob = color('linesColor', '#ff0000')
  const linesWidthKnob = number('linesWidth', 1, {
    range: true, min: 1, max: 5
  })
  const showAreasKnob = boolean('showAreas', true)
  const areasBgColorKnob = color('areasBgColor', '#ff0000')
  const areasOpacityKnob = number('areasOpacity', 50, {
    range: true, min: 1, max: 100
  })
  const areasLineColorKnob = color('areasLineColor', '#ff0000')
  const areasLineWidthKnob = number('areasLineWidth', 1, {
    range: true, min: 1, max: 5
  })

  const exampleProps = {
    title: titleKnob,
    subtitle: subtitleKnob,
    tickInterval: tickIntervalKnob,
    step: stepKnob,
    labelsSize: labelsSizeKnob,
    labelsColor: labelsColorKnob,
    gridColor: gridColorKnob,
    showLines: showLinesKnob,
    linesColor: linesColorKnob,
    linesWidth: linesWidthKnob,
    showAreas: showAreasKnob,
    areasBgColor: areasBgColorKnob,
    areasOpacity: areasOpacityKnob,
    areasLineColor: areasLineColorKnob,
    areasLineWidth: areasLineWidthKnob
  }

  return (
    <div style={{ margin: 20 }}>
      <Example {...exampleProps} />
    </div>
  )
})
