import React from 'react'
import { Provider } from 'react-redux'
import { storiesOf } from '@storybook/react'
import addons from '@storybook/addons'
import withRedux from 'addon-redux/withRedux'

import { Columns, Tabs } from './components'
import { Chart, DataTable } from './containers'
import { Settings } from './layouts'
import store from './store/storybookStore'
import markdown from './App.md'

const withReduxDecorator = withRedux(addons)({ Provider, store })

const stories = storiesOf('App', module)
stories.addDecorator(withReduxDecorator)

stories.add('App', () => {
  const dataTab = {
    title: 'Данные',
    icon: 'database',
    content: <DataTable />
  }

  const settingsTab = {
    title: 'Настройки',
    icon: 'setting',
    content: <Settings />
  }

  return (
    <Columns>
      <Tabs tabs={[dataTab, settingsTab]} />
      <Chart />
    </Columns>
  )
}, {
  notes: {
    markdown
  }
})
