import React from 'react'
import { storiesOf } from '@storybook/react'
import { object, number } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'
import DataTable from './DataTable'

const stories = storiesOf('Components', module)

stories.add('DataTable', () => {
  const valueKnob = object('value', [
    ['15', '100'],
    ['15', '100asd'],
    ['30', '120'],
    ['45', '90'],
    ['60', '110'],
    ['60', ''],
    ['345', '85'],
    ['330', '105'],
    ['330,122', '105'],
    ['315', '70'],
    ['185', '110']
  ])
  const addRowsCountKnob = number('addRowsCount', 10)

  return (
    <div style={{ margin: 20 }}>
      <DataTable
        value={valueKnob}
        addRowsCount={addRowsCountKnob}
        onChange={action('onChange')}
      />
    </div>
  )
})
