import React from 'react'
import { storiesOf } from '@storybook/react'
import { number, text } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'
import { Alert } from 'antd'
import NumberInput from './NumberInput'

const stories = storiesOf('Components', module)

const error = (message) => (
  <Alert
    description={message}
    type="error"
    showIcon
  />
)

stories.add('NumberInput', () => {
  const minKnob = number('min', 0)
  const maxKnob = number('max', 100)
  const valueKnob = number('value', 50)
  const labelKnob = text('label', 'Number Input Label')

  if (minKnob !== 0 && !minKnob) {
    return error(<>The <code>min</code> prop is required</>)
  }

  if (maxKnob !== 0 && !maxKnob) {
    return error(<>The <code>max</code> prop is required</>)
  }

  if (valueKnob !== 0 && !valueKnob) {
    return error(<>The <code>value</code> prop is required</>)
  }

  if (maxKnob <= minKnob) {
    return error(
      <>The <code>max</code> must be larger than the <code>min</code> prop</>
    )
  }

  if (valueKnob < minKnob || valueKnob > maxKnob) {
    return error(
      <>
        The <code>value</code> prop must be between
        {' '}<code>min</code> and <code>max</code>
      </>
    )
  }

  return (
    <div style={{ margin: 20 }}>
      <NumberInput
        min={minKnob}
        max={maxKnob}
        value={valueKnob}
        onChange={action('onChange')}
        label={labelKnob}
      />
    </div>
  )
})
