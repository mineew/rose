import React from 'react'
import { CollapseCard } from '../../components'
import {
  Title,
  Subtitle,
  TickInterval,
  Step,
  LabelsSize,
  GridColor,
  LabelsColor,
  ShowLines,
  LinesWidth,
  LinesColor,
  ShowAreas,
  AreasLineWidth,
  AreasLineColor,
  AreasBgColor,
  AreasOpacity
} from '../../containers'
import './Settings.css'

function Settings () {
  return (
    <div className="Settings">
      {/* --- Title -------------------------------------------------------- */}
      <CollapseCard title="Заголовок" icon="edit">
        <Title />
        <Subtitle />
      </CollapseCard>

      {/* --- Grid --------------------------------------------------------- */}
      <CollapseCard title="Сетка" icon="table">
        <TickInterval />
        <Step />
        <LabelsSize />
        <GridColor />
        <LabelsColor />
      </CollapseCard>

      {/* --- Lines -------------------------------------------------------- */}
      <CollapseCard title="Линии" icon="line-chart">
        <ShowLines />
        <LinesWidth />
        <LinesColor />
      </CollapseCard>

      {/* --- Area --------------------------------------------------------- */}
      <CollapseCard title="Область" icon="area-chart">
        <ShowAreas />
        <AreasLineWidth />
        <AreasLineColor />
        <AreasBgColor />
        <AreasOpacity />
      </CollapseCard>
    </div>
  )
}

export default Settings
