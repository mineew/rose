import * as t from '../actions/types'
import { initialState as initialLastSavedAppData } from './appData'

export const initialState = {
  loading: false,
  error: null,
  filePath: '',
  fileName: '',
  lastSavedAppData: initialLastSavedAppData
}

export default function manage (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case t.FILE_OPEN_START:
    case t.FILE_SAVE_START:
    case t.FILE_SAVE_AS_START:
    case t.IMPORT_START: {
      return {
        ...state,
        loading: true,
        error: null
      }
    }

    case t.FILE_OPEN_ERROR:
    case t.FILE_SAVE_ERROR:
    case t.FILE_SAVE_AS_ERROR:
    case t.IMPORT_ERROR: {
      return {
        ...state,
        loading: false,
        error: payload
      }
    }

    case t.FILE_OPEN_SUCCESS:
    case t.FILE_SAVE_AS_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: null,
        filePath: payload.path,
        fileName: payload.name,
        lastSavedAppData: payload.data
      }
    }

    case t.FILE_SAVE_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: null,
        lastSavedAppData: payload.data
      }
    }

    case t.FILE_CLOSE:
    case t.IMPORT_SUCCESS: {
      return initialState
    }

    default: {
      return state
    }
  }
}
