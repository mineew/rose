/** @typedef {import('../../components/FileMenu/FileMenu').FileMenuProps} FileMenuProps */

import { connect } from 'react-redux'
import { manage, haveUnsavedChanges } from '../../store/selectors'
import {
  fileOpenRequest,
  fileSaveAsRequest,
  fileSaveStart,
  fileClose,
  importRequest,
  quit
} from '../../actions'
import { FileMenu } from '../../components'

/** @returns {FileMenuProps} */
export function mapStateToProps (state) {
  return {
    fileName: manage(state).fileName,
    needSave: haveUnsavedChanges(state)
  }
}

/** @returns {FileMenuProps} */
export function mapDispatchToProps (dispatch) {
  return {
    onFileOpenClick: () => dispatch(fileOpenRequest()),
    onFileCloseClick: () => dispatch(fileClose()),
    onFileSaveClick: () => dispatch(fileSaveStart()),
    onSaveAsClick: () => dispatch(fileSaveAsRequest()),
    onImportClick: () => dispatch(importRequest()),
    onQuitClick: () => dispatch(quit())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FileMenu)
