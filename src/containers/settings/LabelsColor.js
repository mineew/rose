import { TextInput } from '../../components'
import { setLabelsColor } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  TextInput,
  (state) => state.labelsColor,
  setLabelsColor,
  {
    label: 'Цвет подписей',
    type: 'color'
  }
)
