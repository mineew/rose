import React from 'react'
import PropTypes from 'prop-types'
import { Switch } from 'antd'
import './CheckInput.css'

/**
 * @typedef {Object} CheckInputProps
 * @property {boolean} value
 * @property {function(boolean)} onChange
 * @property {string} [label]
 */

/**
 * @param {CheckInputProps} props
 * @returns {React.Component<CheckInputProps>}
 */
function CheckInput (props) {
  const { value, onChange, label } = props

  const handleChange = (checked) => {
    onChange(checked)
  }

  return (
    <label className="CheckInput">
      <Switch checked={value} onChange={handleChange} />
      {Boolean(label) && (
        <span className="CheckInput__label">
          {label}
        </span>
      )}
    </label>
  )
}

CheckInput.defaultProps = {
  label: ''
}

CheckInput.propTypes = {
  value: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string
}

export default CheckInput
