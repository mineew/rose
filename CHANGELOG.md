# 1.0.0 (May 8, 2019)

## 1.1.3 (June 4, 2019)

* Hide menubar in production.

## 1.1.2 (June 3, 2019)

* Reflow only the first time.

## 1.1.1 (May 22, 2019)

* Added `release` stage to CI.

## 1.1.0 (May 10, 2019)

* Added `ErrorBoundary`.

## 1.0.0 (May 8, 2019)

* Rose is released.
