import React from 'react'
import { render } from 'react-testing-library'
import ErrorBoundary from './ErrorBoundary'

const SomeBuggyComponent = () => {
  throw new Error('It does not look good')
}

describe('<ErrorBoundary />', () => {
  it('catches errors', () => {
    // mute jsdom's console
    jest.spyOn(window._virtualConsole, 'emit').mockImplementation(() => false)

    jest.spyOn(ErrorBoundary, 'getDerivedStateFromError')
    render(
      <ErrorBoundary>
        <SomeBuggyComponent />
      </ErrorBoundary>
    )
    expect(ErrorBoundary.getDerivedStateFromError).toHaveBeenCalled()
  })
})
