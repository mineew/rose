import React from 'react'
import { render } from 'react-testing-library'
import Columns from './Columns'

describe('<Columns />', () => {
  const leftContent = 'Left Content'
  const rightContent = 'Right Content'

  const wrapper = () => render(
    <Columns>
      <p>{leftContent}</p>
      <p>{rightContent}</p>
    </Columns>
  )

  it('renders left column', () => {
    const { baseElement } = wrapper()
    expect(baseElement.innerHTML).toMatch(leftContent)
  })

  it('renders right column', () => {
    const { baseElement } = wrapper()
    expect(baseElement.innerHTML).toMatch(rightContent)
  })
})
