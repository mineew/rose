import addons from '@storybook/addons'
import '@storybook/addon-knobs/register'
import '@storybook/addon-actions/register'
import '@storybook/addon-storysource/register'
import '@storybook/addon-notes/register'
import registerRedux from 'addon-redux/register'

registerRedux(addons)
