const webpack = require('webpack')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
const ErrorOverlayWebpackPlugin = require('error-overlay-webpack-plugin')

/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @param {Configuration} base
 * @returns {Configuration}
 */
module.exports = (base) => {
  const isProduction = base.mode === 'production'
  const port = 3000

  if (isProduction) {
    return {}
  }

  return {
    devServer: {
      port,
      quiet: true,
      overlay: {
        errors: true,
        warnings: true
      },
      hot: true
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new FriendlyErrorsWebpackPlugin({
        compilationSuccessInfo: {
          messages: [
            `Project is running at http://localhost:${port}/\n`
          ]
        }
      }),
      new ErrorOverlayWebpackPlugin()
    ]
  }
}
