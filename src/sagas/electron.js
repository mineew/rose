/** @type {import('electron').AllElectron} */
const electron = window.electron
const { remote } = electron

const pt = remote.require('path')
const fs = remote.require('fs')
/** @type {import('exceljs').Workbook} */
const Workbook = remote.require('exceljs').Workbook

/**
 * @param {{name: string, extensions: Array<string>}} filter
 * @returns {Promise<string>}
 */
export function getFileOpenPath (filter) {
  const options = {
    properties: ['openFile'],
    filters: [filter]
  }

  return new Promise((resolve) => {
    remote.dialog.showOpenDialog(options, (paths) => {
      if (paths && paths.length === 1) {
        resolve(paths[0])
      }
      resolve('')
    })
  })
}

/**
 * @param {{name: string, extensions: Array<string>}} filter
 * @returns {Promise<string>}
 */
export function getFileSavePath (filter) {
  const options = {
    filters: [filter]
  }

  return new Promise((resolve) => {
    remote.dialog.showSaveDialog(options, (path) => {
      resolve(path)
    })
  })
}

/**
 * @param {string} path
 * @returns {Promise<string>}
 */
export function readFile (path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (error, data) => {
      if (error) {
        reject(error)
      } else {
        resolve(data)
      }
    })
  })
}

/**
 * @param {string} path
 * @param {string} data
 * @returns {Promise}
 */
export function saveFile (path, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, (error) => {
      if (error) {
        reject(error)
      } else {
        resolve()
      }
    })
  })
}

/**
 * @param {string} path
 * @returns {string}
 */
export function getFileName (path) {
  return pt.basename(path)
}

/**
 * @param {object} data
 * @returns {string}
 */
export function serializeData (data) {
  return JSON.stringify(data, undefined, 2)
}

/**
 * @param {string} data
 * @returns {object}
 */
export function deserializeData (data) {
  return JSON.parse(data)
}

export async function importFromExcel (path) {
  const workbook = new Workbook()
  await workbook.xlsx.readFile(path)
  const worksheet = workbook.getWorksheet(1)

  const chartData = []
  let row = 1

  while (true) {
    const angle = worksheet.getCell(row, 1).text.trim()
    const value = worksheet.getCell(row, 2).text.trim()

    if (!angle || !value) {
      break
    }

    chartData.push([angle, value])

    row += 1
  }

  return chartData
}

export function quit () {
  remote.app.quit()
}
