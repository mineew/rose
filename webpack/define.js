const fs = require('fs')
const dotenv = require('dotenv')
const webpack = require('webpack')

const envPath = '.app.env'
const envBuffer = fs.readFileSync(envPath)
const env = dotenv.parse(envBuffer)

/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @param {Configuration} base
 * @returns {Configuration}
 */
module.exports = (base) => {
  const { mode } = base

  return {
    plugins: [
      new webpack.DefinePlugin({
        'process.env': JSON.stringify({
          NODE_ENV: mode,
          ...env
        })
      })
    ]
  }
}
