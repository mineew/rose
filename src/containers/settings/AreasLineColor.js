import { TextInput } from '../../components'
import { setAreasLineColor } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  TextInput,
  (state) => state.areasLineColor,
  setAreasLineColor,
  {
    label: 'Цвет обводки области',
    type: 'color'
  }
)
