import { combineReducers } from 'redux'
import appData from './appData'
import manage from './manage'

export default combineReducers({
  appData,
  manage
})
