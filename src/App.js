import React, { Fragment } from 'react'
import { Columns, Tabs } from './components'
import { Chart, DataTable } from './containers'
import { Settings, Menu } from './layouts'
import './App.css'

const dataTab = {
  title: 'Данные',
  icon: 'database',
  content: <DataTable />
}

const settingsTab = {
  title: 'Настройки',
  icon: 'setting',
  content: <Settings />
}

function App () {
  return (
    <Fragment>
      <Menu />
      <Columns>
        <Tabs tabs={[dataTab, settingsTab]} />
        <Chart />
      </Columns>
    </Fragment>
  )
}

export default App
