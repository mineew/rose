import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Icon } from 'antd'
import { angleIsValid, valueIsValid } from '../Chart/drawChart'
import './DataPointInput.css'

/**
 * @typedef {Object} DataPointInputProps
 * @property {[string, string]} value
 * @property {function([string, string])} onChange
 */

/**
 * @param {DataPointInputProps} props
 * @returns {React.Component<DataPointInputProps>}
 */
function DataPointInput (props) {
  const { value: inputValue, onChange } = props
  const [ angle, value ] = inputValue

  const showStatus = angle !== '' || value !== ''
  const hasAngleError = !angleIsValid(angle)
  const hasValueError = !valueIsValid(value)

  const status = showStatus
    ? (hasAngleError || hasValueError ? 'error' : 'success')
    : undefined

  const angleIcon = showStatus
    ? renderIcon(hasAngleError ? 'error' : 'success')
    : <span />

  const valueIcon = showStatus
    ? renderIcon(hasValueError ? 'error' : 'success')
    : <span />

  /** @param {React.ChangeEvent<HTMLInputElement>} e */
  const handleAngleChange = (e) => {
    onChange([e.target.value, value])
  }

  /** @param {React.ChangeEvent<HTMLInputElement>} e */
  const handleValueChange = (e) => {
    onChange([angle, e.target.value])
  }

  return (
    <Form.Item className="DataPointInput" validateStatus={status}>
      <Input.Group compact={true}>
        <Input
          value={angle}
          onChange={handleAngleChange}
          style={{ width: '50%' }}
          suffix={angleIcon}
        />
        <Input
          value={value}
          onChange={handleValueChange}
          style={{ width: '50%' }}
          suffix={valueIcon}
        />
      </Input.Group>
    </Form.Item>
  )
}

DataPointInput.propTypes = {
  value: PropTypes.arrayOf(PropTypes.string).isRequired,
  onChange: PropTypes.func.isRequired
}

export default DataPointInput

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {'error'|'success'} type
 */
function renderIcon (type) {
  return type === 'error' ? (
    <Icon
      className="suffix-icon"
      type="close-circle"
      theme="twoTone"
      twoToneColor="red"
    />
  ) : (
    <Icon
      className="suffix-icon suffix-icon-success"
      type="check"
    />
  )
}
