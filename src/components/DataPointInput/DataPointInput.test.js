/** @typedef {import('./DataPointInput').DataPointInputProps} DataPointInputProps */

import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import DataPointInput from './DataPointInput'

/** @type {DataPointInputProps} */
const commonProps = {
  value: ['10', '100'],
  onChange: jest.fn()
}

/** @param {DataPointInputProps} testProps */
const wrapper = (testProps) => {
  const props = { ...commonProps, ...testProps }
  return render(
    <DataPointInput {...props} />
  )
}

describe('<DataPointInput />', () => {
  it('shows success status', () => {
    const { baseElement: { innerHTML: html } } = wrapper()
    expect(html).toMatch('has-success')
  })

  it('shows error status', () => {
    const {
      baseElement: { innerHTML: angleErrorHtml }
    } = wrapper({ value: ['378', '100'] })
    expect(angleErrorHtml).toMatch('has-error')
    const {
      baseElement: { innerHTML: valueErrorHtml }
    } = wrapper({ value: ['10', '100asd'] })
    expect(valueErrorHtml).toMatch('has-error')
  })

  it('does not render status if there is no data', () => {
    const { baseElement } = wrapper({ value: ['', ''] })
    const html = baseElement.innerHTML
    expect(html).not.toMatch('has-success')
    expect(html).not.toMatch('has-error')
  })

  it('changes values', () => {
    const { getByDisplayValue } = wrapper()
    fireEvent.change(getByDisplayValue(commonProps.value[0]), {
      target: { value: commonProps.value[0] + '1' }
    })
    expect(commonProps.onChange).toHaveBeenCalledTimes(1)
    expect(commonProps.onChange).toHaveBeenLastCalledWith([
      commonProps.value[0] + '1',
      commonProps.value[1]
    ])
    fireEvent.change(getByDisplayValue(commonProps.value[1]), {
      target: { value: commonProps.value[1] + '1' }
    })
    expect(commonProps.onChange).toHaveBeenCalledTimes(2)
    expect(commonProps.onChange).toHaveBeenLastCalledWith([
      commonProps.value[0],
      commonProps.value[1] + '1'
    ])
  })
})
