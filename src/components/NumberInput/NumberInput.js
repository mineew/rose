import React from 'react'
import PropTypes from 'prop-types'
import { Slider } from 'antd'
import './NumberInput.css'

/**
 * @typedef {Object} NumberInputProps
 * @property {number} min
 * @property {number} max
 * @property {number} value
 * @property {function(number)} onChange
 * @property {string} [label]
 */

/**
 * @param {NumberInputProps} props
 * @returns {React.Component<NumberInputProps>}
 */
function NumberInput (props) {
  const { min, max, value, onChange, label } = props

  return (
    <label className="NumberInput">
      {Boolean(label) && (
        <span className="NumberInput__label">
          {label}
        </span>
      )}
      <Slider
        min={min}
        max={max}
        value={value}
        onChange={onChange}
      />
    </label>
  )
}

NumberInput.defaultProps = {
  label: ''
}

NumberInput.propTypes = {
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string
}

export default NumberInput
