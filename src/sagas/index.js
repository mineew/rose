import { all } from 'redux-saga/effects'
import manageSagas from './manageSagas'
import notificationSagas from './notificationSagas'

export default function * () {
  yield all([
    manageSagas(),
    notificationSagas()
  ])
}
