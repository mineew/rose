import React from 'react'
import PropTypes from 'prop-types'
import { Menu, Icon, Badge, Modal } from 'antd'

/**
 * @typedef {Object} FileDropdownMenuProps
 * @property {boolean} fileIsOpened
 * @property {boolean} needSave
 * @property {function} onFileOpenClick
 * @property {function} onFileCloseClick
 * @property {function} onFileSaveClick
 * @property {function} onSaveAsClick
 * @property {function} onImportClick
 * @property {function} onQuitClick
 */

/**
 * @param {FileDropdownMenuProps} props
 * @returns {React.Component<FileDropdownMenuProps>}
 */
export function FileDropdownMenu (props) {
  const {
    fileIsOpened,
    needSave,
    onFileOpenClick,
    onFileCloseClick,
    onFileSaveClick,
    onSaveAsClick,
    onImportClick,
    onQuitClick
  } = props

  const handleFileOpen = () => {
    const title = 'Вы действительно хотите открыть файл?'
    const description = 'Несохраненные данные будут потеряны.'
    const action = 'Открыть'

    needSave
      ? confirm(title, description, action, onFileOpenClick)
      : onFileOpenClick()
  }

  const handleFileClose = () => {
    const title = 'Вы действительно хотите закрыть файл?'
    const description = 'Несохраненные данные будут потеряны.'
    const action = 'Закрыть'

    needSave
      ? confirm(title, description, action, onFileCloseClick)
      : onFileCloseClick()
  }

  const handleFileSave = () => {
    onFileSaveClick()
  }

  const handleSaveAs = () => {
    onSaveAsClick()
  }

  const handleImport = () => {
    const title = 'Вы действительно хотите импортировать данные?'
    const description = 'Несохраненные данные будут потеряны.'
    const action = 'Импортировать'

    needSave
      ? confirm(title, description, action, onImportClick)
      : onImportClick()
  }

  const handleQuit = () => {
    const title = 'Вы действительно хотите выйти?'
    const description = 'Несохраненные данные будут потеряны.'
    const action = 'Выйти'

    needSave
      ? confirm(title, description, action, onQuitClick)
      : onQuitClick()
  }

  return (
    <Menu>
      <Menu.Item
        disabled={fileIsOpened}
        onClick={handleFileOpen}
      >
        <Icon type="folder-open" /> Открыть файл...
      </Menu.Item>
      <Menu.Item
        disabled={!fileIsOpened}
        onClick={handleFileClose}
      >
        <Icon type="close" /> Закрыть файл
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item
        disabled={!fileIsOpened || !needSave}
        onClick={handleFileSave}
      >
        {fileIsOpened && needSave && (
          <Badge status="default" color="red" />
        )}
        <Icon type="file-sync" /> Сохранить
      </Menu.Item>
      <Menu.Item
        disabled={!needSave}
        onClick={handleSaveAs}
      >
        {needSave && (
          <Badge status="default" color="red" />
        )}
        <Icon type="save" /> Сохранить как...
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item
        disabled={fileIsOpened}
        onClick={handleImport}
      >
        <Icon type="file-excel" /> Импорт из Excel
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item
        onClick={handleQuit}
      >
        <Icon type="logout" /> Выход
      </Menu.Item>
    </Menu>
  )
}

FileDropdownMenu.propTypes = {
  fileIsOpened: PropTypes.bool.isRequired,
  needSave: PropTypes.bool.isRequired,
  onFileOpenClick: PropTypes.func.isRequired,
  onFileCloseClick: PropTypes.func.isRequired,
  onFileSaveClick: PropTypes.func.isRequired,
  onSaveAsClick: PropTypes.func.isRequired,
  onImportClick: PropTypes.func.isRequired,
  onQuitClick: PropTypes.func.isRequired
}

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {React.ReactNode} title
 * @param {React.ReactNode} description
 * @param {React.ReactNode} okText
 * @param {function} callback
 */
function confirm (title, description, okText, callback) {
  const modal = Modal.confirm({
    title,
    content: description,
    onOk: () => {
      callback()
      modal.destroy()
    },
    okText,
    cancelText: 'Отмена'
  })
}
