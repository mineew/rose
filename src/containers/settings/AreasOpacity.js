import { NumberInput } from '../../components'
import { setAreasOpacity } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  NumberInput,
  (state) => state.areasOpacity,
  setAreasOpacity,
  {
    label: 'Прозрачность области',
    min: 0,
    max: 100
  }
)
