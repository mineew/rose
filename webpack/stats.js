const WebpackBar = require('webpackbar')

/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @param {Configuration} base
 * @returns {Configuration}
 */
module.exports = (base) => {
  const isProduction = base.mode === 'production'

  if (!isProduction) {
    return {}
  }

  return {
    stats: {
      all: false,
      errors: true
    },
    plugins: [
      new WebpackBar()
    ]
  }
}
