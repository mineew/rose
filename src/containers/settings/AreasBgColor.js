import { TextInput } from '../../components'
import { setAreasBgColor } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  TextInput,
  (state) => state.areasBgColor,
  setAreasBgColor,
  {
    label: 'Цвет области',
    type: 'color'
  }
)
