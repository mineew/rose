import { mapStateToProps } from './Chart'

describe('<Chart /> container', () => {
  it('maps prepared chart data and settings from state to props', () => {
    const state = {
      appData: {
        chartData: [
          ['15', '100']
        ],
        settings: {
          step: 3
        }
      }
    }
    const props = {
      data: [[105, 100]],
      step: 3
    }
    expect(mapStateToProps(state)).toEqual(props)
  })
})
