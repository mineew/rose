/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @param {Configuration} base
 * @returns {Configuration}
 */
module.exports = (base) => {
  const hashLength = base.output.hashDigestLength

  const file = {
    loader: 'file-loader',
    options: {
      name: `fonts/[name].[contenthash:${hashLength}].[ext]`
    }
  }

  return {
    module: {
      rules: [{
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [file]
      }]
    }
  }
}
