/** @typedef {import('./FileMenu').FileMenuProps} FileMenuProps */

import React from 'react'
import { render } from 'react-testing-library'
import FileMenu from './FileMenu'

/** @param {FileMenuProps} props */
const wrapper = (props = {}) => {
  return render(
    <FileMenu {...props} />
  )
}

describe('<FileMenu />', () => {
  it('renders', () => {
    wrapper()
  })

  it('renders a filename', () => {
    const fileName = 'some-file.ext'
    const { container } = wrapper({ fileName })
    expect(container.innerHTML).toMatch(fileName)
  })

  it('provides default callbacks', () => {
    FileMenu.defaultProps.onFileCloseClick()
    FileMenu.defaultProps.onFileOpenClick()
    FileMenu.defaultProps.onFileSaveClick()
    FileMenu.defaultProps.onImportClick()
    FileMenu.defaultProps.onQuitClick()
    FileMenu.defaultProps.onSaveAsClick()
  })
})
