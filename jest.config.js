module.exports = {
  verbose: true,
  collectCoverageFrom: [
    '<rootDir>/src/**/*.js'
  ],
  coveragePathIgnorePatterns: [
    '.stories.js',
    'index.js'
  ],
  setupFilesAfterEnv: [
    '<rootDir>/setupTests.js'
  ],
  moduleNameMapper: {
    '\\.css$': 'identity-obj-proxy'
  }
}
