/** @typedef {import('./DataTable').DataTableProps} DataTableProps */

import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import DataTable from './DataTable'

/** @type {DataTableProps} */
const commonProps = {
  value: [
    ['15', '100'],
    ['30', '120'],
    ['45', '90'],
    ['60', '110'],
    ['345', '85'],
    ['330', '105'],
    ['315', '70']
  ],
  onChange: jest.fn(),
  addRowsCount: 1
}

afterEach(() => {
  commonProps.onChange.mockClear()
})

/** @param {DataTableProps} testProps */
const wrapper = (testProps) => {
  const props = { ...commonProps, ...testProps }
  return render(
    <DataTable {...props} />
  )
}

describe('<DataTable />', () => {
  it('renders data points', () => {
    const { baseElement: { innerHTML: html } } = wrapper()
    for (let [ angle, value ] of commonProps.value) {
      expect(html).toMatch(angle)
      expect(html).toMatch(value)
    }
  })

  it('renders placeholder if there is no data', () => {
    const { baseElement: { innerHTML: html } } = wrapper({ value: [] })
    expect(html).not.toBeNull()
  })

  it('changes data points', () => {
    const { getByDisplayValue } = wrapper()
    const input = getByDisplayValue(commonProps.value[0][0])
    const changedValue = commonProps.value[0][0] + '1'
    fireEvent.change(input, {
      target: { value: changedValue }
    })
    expect(commonProps.onChange).toHaveBeenCalledTimes(1)
    expect(commonProps.onChange).toHaveBeenLastCalledWith([
      [changedValue, commonProps.value[0][1]],
      ...commonProps.value.slice(1)
    ])
  })

  it('deletes data points', () => {
    const { baseElement } = wrapper()
    const deleteButtons = baseElement.querySelectorAll('.ant-btn-dashed')
    fireEvent.click(deleteButtons.item(0))
    expect(commonProps.onChange).toHaveBeenCalledTimes(1)
    expect(commonProps.onChange).toHaveBeenLastCalledWith(
      commonProps.value.slice(1)
    )
  })

  it('adds data points', () => {
    const { baseElement } = wrapper()
    const addButtons = baseElement.querySelectorAll('.ant-btn-primary')
    fireEvent.click(addButtons.item(0))
    expect(commonProps.onChange).toHaveBeenCalledTimes(1)
    expect(commonProps.onChange).toHaveBeenLastCalledWith([
      ...commonProps.value,
      ...Array(commonProps.addRowsCount).fill(['', ''])
    ])
  })
})
