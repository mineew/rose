/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @param {Configuration} base
 * @returns {Configuration}
 */
module.exports = (base) => {
  const hashLength = base.output.hashDigestLength

  const url = {
    loader: 'url-loader',
    options: {
      limit: 10 * 1000,
      name: `images/[name].[contenthash:${hashLength}].[ext]`
    }
  }

  return {
    module: {
      rules: [{
        test: /\.(png|jpe?g)$/,
        use: [url]
      }]
    }
  }
}
