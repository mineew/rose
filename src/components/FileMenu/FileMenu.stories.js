import React from 'react'
import { storiesOf } from '@storybook/react'
import { text, boolean } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'
import FileMenu from './FileMenu'

const stories = storiesOf('Components', module)

stories.add('FileMenu', () => {
  const fileNameKnob = text('fileName', '')
  const needSaveKnob = boolean('needSave', false)

  return (
    <div style={{ margin: 20 }}>
      <FileMenu
        fileName={fileNameKnob}
        needSave={needSaveKnob}
        onFileOpenClick={action('onFileOpenClick')}
        onFileCloseClick={action('onFileCloseClick')}
        onFileSaveClick={action('onFileSaveClick')}
        onSaveAsClick={action('onSaveAsClick')}
        onQuitClick={action('onQuitClick')}
      />
    </div>
  )
})
