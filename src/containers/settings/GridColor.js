import { TextInput } from '../../components'
import { setGridColor } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  TextInput,
  (state) => state.gridColor,
  setGridColor,
  {
    label: 'Цвет сетки',
    type: 'color'
  }
)
