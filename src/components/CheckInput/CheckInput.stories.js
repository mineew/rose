import React from 'react'
import { storiesOf } from '@storybook/react'
import { boolean, text } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'
import CheckInput from './CheckInput'

const stories = storiesOf('Components', module)

stories.add('CheckInput', () => {
  const valueKnob = boolean('value', false)
  const labelKnob = text('label', 'Check Input Label')

  return (
    <div style={{ margin: 20 }}>
      <CheckInput
        value={valueKnob}
        onChange={action('onChange')}
        label={labelKnob}
      />
    </div>
  )
})
