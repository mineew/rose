import { connect } from 'react-redux'
import { Chart } from '../../components'
import { settings, preparedChartData } from '../../store/selectors'

export function mapStateToProps (state) {
  return {
    ...settings(state),
    data: preparedChartData(state)
  }
}

export default connect(
  mapStateToProps
)(Chart)
