import { NumberInput } from '../../components'
import { setLabelsSize } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  NumberInput,
  (state) => state.labelsSize,
  setLabelsSize,
  {
    label: 'Размер подписей',
    min: 8,
    max: 30
  }
)
