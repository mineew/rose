FROM node

WORKDIR /rose

COPY package.json yarn.lock ./
RUN yarn

COPY . ./

CMD ["yarn", "pipeline"]
