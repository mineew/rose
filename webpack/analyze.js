const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')

/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @param {Configuration} base
 * @returns {Configuration}
 */
module.exports = (base) => {
  const isProduction = base.mode === 'production'

  const analyzePlugin = isProduction && new BundleAnalyzerPlugin({
    analyzerMode: 'static',
    generateStatsFile: true,
    reportFilename: '.stats/report.html',
    statsFilename: '.stats/stats.json',
    openAnalyzer: false,
    logLevel: 'warn'
  })

  return {
    plugins: [analyzePlugin].filter(Boolean)
  }
}
