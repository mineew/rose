import deepFreeze from 'deep-freeze'
import * as t from '../actions/types'
import manage, { initialState } from './manage'

describe('manage reducer', () => {
  it('returns the initial state', () => {
    const reducedState = manage(undefined, { type: 'SOME_UNEXPECTED_ACTION' })
    expect(reducedState).toEqual(initialState)
  })

  it('handles `*_START` actions', () => {
    const action = { type: t.FILE_OPEN_START }
    const stateBefore = initialState
    const stateAfter = { ...initialState, loading: true, error: null }

    deepFreeze(stateBefore)

    const reducedState = manage(stateBefore, action)
    expect(reducedState).toEqual(stateAfter)
  })

  it('handles `*_ERROR` actions', () => {
    const error = new Error('some error message')
    const action = { type: t.FILE_OPEN_ERROR, payload: error, error: true }
    const stateBefore = initialState
    const stateAfter = { ...initialState, loading: false, error }

    deepFreeze(stateBefore)

    const reducedState = manage(stateBefore, action)
    expect(reducedState).toEqual(stateAfter)
  })

  it('handles "change file" actions', () => {
    const action = {
      type: t.FILE_OPEN_SUCCESS,
      payload: {
        path: '/some/path',
        name: 'file-name.json',
        data: '{ some: "data" }'
      }
    }
    const stateBefore = initialState
    const stateAfter = {
      ...initialState,
      loading: false,
      error: null,
      filePath: action.payload.path,
      fileName: action.payload.name,
      lastSavedAppData: action.payload.data
    }

    deepFreeze(stateBefore)

    const reducedState = manage(stateBefore, action)
    expect(reducedState).toEqual(stateAfter)
  })

  it('handles "save file" actions', () => {
    const action = {
      type: t.FILE_SAVE_SUCCESS,
      payload: { data: '{ some: "data" }' }
    }
    const stateBefore = initialState
    const stateAfter = {
      ...initialState,
      loading: false,
      error: null,
      lastSavedAppData: action.payload.data
    }

    deepFreeze(stateBefore)

    const reducedState = manage(stateBefore, action)
    expect(reducedState).toEqual(stateAfter)
  })

  it('handles "new file" actions', () => {
    const action = { type: t.FILE_CLOSE }
    const stateBefore = initialState
    const stateAfter = { ...initialState }

    deepFreeze(stateBefore)

    const reducedState = manage(stateBefore, action)
    expect(reducedState).toEqual(stateAfter)
  })
})
