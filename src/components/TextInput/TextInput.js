import React from 'react'
import PropTypes from 'prop-types'
import { Input } from 'antd'
import './TextInput.css'

/**
 * @typedef {Object} TextInputProps
 * @property {string} value
 * @property {function(string)} onChange
 * @property {string} [label]
 * @property {string} [type]
 */

/**
 * @param {TextInputProps} props
 * @returns {React.Component<TextInputProps>}
 */
function TextInput (props) {
  const { value, onChange, label, type } = props

  /** @param {React.ChangeEvent<HTMLInputElement>} e */
  const handleChange = (e) => {
    onChange(e.target.value)
  }

  return (
    <label className="TextInput">
      {Boolean(label) && (
        <span className="TextInput__label">
          {label}
        </span>
      )}
      <Input
        type={type}
        value={value}
        onChange={handleChange}
      />
    </label>
  )
}

TextInput.defaultProps = {
  label: '',
  type: 'text'
}

TextInput.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string,
  type: PropTypes.string
}

export default TextInput
