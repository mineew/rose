[![pipeline status](https://gitlab.com/mineew/rose/badges/master/pipeline.svg)](https://gitlab.com/mineew/rose/commits/master)
[![coverage report](https://gitlab.com/mineew/rose/badges/master/coverage.svg)](https://gitlab.com/mineew/rose/commits/master)
[![Storybook](https://cdn.jsdelivr.net/gh/storybooks/brand@master/badge/badge-storybook.svg)](https://mineew.gitlab.io/rose)

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

# Rose

Rose is a desktop application built with Electron, React and Redux.

![Screenshot](/screenshot.png)

Rose builds rose-charts using data entered manually or imported from an Excel
file.

## Storybook

You can view Rose's storybook [here](https://mineew.gitlab.io/rose).

## Download

You can download the latest build for *Windows* [here](https://gitlab.com/mineew/rose/-/jobs/artifacts/master/download?job=release).

## Manual build

To build the application on Linux or Windows, type the following in your
terminal:

```
$ mkdir rose && cd rose
$ git clone https://gitlab.com/mineew/rose.git .
$ yarn
$ yarn build
```

You can then find the build in the `dist` folder.

## Develop

To install and run the application in development mode, type the following in
your terminal:

```
$ mkdir rose && cd rose
$ git clone https://gitlab.com/mineew/rose.git .
$ yarn
$ yarn start
```

## All available scripts

| NPM Script       | Description                             |
| ---------------- | --------------------------------------- |
| `yarn start`     | runs the application                    |
| `yarn test`      | tests the application                   |
| `yarn cover`     | shows the code coverage                 |
| `yarn lint`      | lints the code                          |
| `yarn build`     | creates a production build              |
| `yarn clear`     | removes the temporary folders           |
| `yarn storybook` | runs the UI development environment     |

## Frameworks and tools

This application is built with the following frameworks and tools:

* [Electron](https://electronjs.org/) - Build cross platform desktop apps with
  JavaScript, HTML, and CSS;
* [React](https://reactjs.org/) - A JavaScript library for building user
  interfaces;
* [Redux](https://redux.js.org/) - A predictable state container for JavaScript
  apps;
* [Create React App](https://github.com/facebook/create-react-app) - create
  react apps with no build configuration;
* [Ant Design of React](https://ant.design/docs/react/introduce) - a React UI
  library that contains a set of high quality components and demos for building
  rich, interactive user interfaces.

The tests are written with these tools:

* [Jest](https://jestjs.io/) - Delightful JavaScript Testing;
* [react-testing-library](https://github.com/testing-library/react-testing-library) -
  Simple and complete React DOM testing utilities that encourage good testing
  practices.

Other tools worth mentioning:

* [Highcharts](https://www.highcharts.com/) - Interactive JavaScript charts for
  your webpage;
* [ExcelJS](https://github.com/exceljs/exceljs) - Read, manipulate and write
  spreadsheet data and styles to XLSX and JSON;
* [redux-saga](https://redux-saga.js.org/) - An alternative side effect model
  for Redux apps;
* [reselect](https://github.com/reduxjs/reselect) - Selector library for Redux.
