import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-testing-library'
import store from './store'
import App from './App'

describe('<App />', () => {
  it('renders', () => {
    render(
      <Provider store={store}>
        <App />
      </Provider>
    )
  })
})
