import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-testing-library'
import store from '../../store'
import Settings from './Settings'

describe('<Settings />', () => {
  it('renders', () => {
    render(
      <Provider store={store}>
        <Settings />
      </Provider>
    )
  })
})
