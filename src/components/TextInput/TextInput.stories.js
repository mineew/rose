import React from 'react'
import { storiesOf } from '@storybook/react'
import { text, select } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'
import TextInput from './TextInput'

const stories = storiesOf('Components', module)

stories.add('TextInput', () => {
  const valueKnob = text('value', '')
  const labelKnob = text('label', 'Text Input Label')
  const typeKnob = select('type', [
    'text',
    'color'
  ])

  return (
    <div style={{ margin: 20 }}>
      <TextInput
        value={valueKnob}
        onChange={action('onChange')}
        label={labelKnob}
        type={typeKnob}
      />
    </div>
  )
})
