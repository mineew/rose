import {
  getFileOpenPath,
  getFileSavePath,
  readFile,
  saveFile,
  getFileName,
  serializeData,
  deserializeData,
  importFromExcel,
  quit
} from './electron'
import { jsonFileFilter } from './manageSagas'

/** @type {import('electron').AllElectron} */
const electron = window.electron
const { remote } = electron

JSON.stringify = jest.fn()
JSON.parse = jest.fn()

describe('electron bindings', () => {
  describe('getFileOpenPath', () => {
    it('resolves with the path', () => {
      expect(getFileOpenPath(jsonFileFilter)).resolves.toBe(
        global.showOpenDialogPaths[0]
      )
    })

    it('resolves with the empty string if there is no path', () => {
      global.showOpenDialogPaths = []

      expect(getFileOpenPath(jsonFileFilter)).resolves.toBe('')
    })
  })

  describe('getFileSavePath', () => {
    it('resolves with the path', () => {
      expect(getFileSavePath(jsonFileFilter)).resolves.toBe(
        global.showSaveDialogPath
      )
    })
  })

  describe('readFile', () => {
    it('resolves with the file data', () => {
      global.readFileData = 'some data'

      expect(readFile('/some/path')).resolves.toBe(global.readFileData)
    })

    it('throws an erros', () => {
      global.readFileError = new Error('some error message')

      expect(readFile('/some/path')).rejects.toEqual(global.readFileError)
    })
  })

  describe('saveFile', () => {
    it('resolves', () => {
      expect(saveFile('/some/path')).resolves.toBeUndefined()
    })

    it('throws an erros', () => {
      global.writeFileError = new Error('some error message')

      expect(saveFile('/some/path')).rejects.toEqual(global.writeFileError)
    })
  })

  describe('getFileName', () => {
    it('returns the file name', () => {
      expect(getFileName('/some/path/to/file')).toBe('file')
    })
  })

  describe('serializeData', () => {
    it('serializes the data', () => {
      serializeData({ some: 'data' })
      expect(JSON.stringify).toHaveBeenCalledTimes(1)
    })
  })

  describe('deserializeData', () => {
    it('deserializes the data', () => {
      deserializeData('{ "some": "data" }')
      expect(JSON.parse).toHaveBeenCalledTimes(1)
    })
  })

  describe('importFromExcel', () => {
    it('reads chart data from excel workbook', () => {
      expect(importFromExcel('/some/path')).resolves.toEqual(global.excelData)
    })
  })

  describe('quit', () => {
    it('quits the app', () => {
      quit()
      expect(remote.app.quit).toHaveBeenCalledTimes(1)
    })
  })
})
