import { put, call, select, takeLatest, all } from 'redux-saga/effects'
import * as t from '../actions/types'
import * as a from '../actions/creators'
import * as electron from './electron'
import manageSagas, {
  openFile,
  saveAsFile,
  saveFile,
  importChartData,
  quit,
  jsonFileFilter,
  excelFileFilter
} from './manageSagas'

describe('manage sagas', () => {
  describe('openFile', () => {
    it('opens a file with the system dialog', () => {
      const path = '/some/path'
      const data = '{ "some": "data" }'

      const generator = openFile()
      // it opens the system dialog
      expect(generator.next().value).toEqual(
        call(electron.getFileOpenPath, jsonFileFilter)
      )
      // it dispatches the `fileOpenStart` action
      expect(generator.next(path).value).toEqual(
        put(a.fileOpenStart())
      )
      // it reads the file
      expect(generator.next().value).toEqual(
        call(electron.readFile, path)
      )
      // it dispatches the `fileOpenSuccess` action
      expect(generator.next(data).value).toEqual(
        put(a.fileOpenSuccess(
          path,
          electron.getFileName(path),
          electron.deserializeData(data)
        ))
      )
    })

    it('do nothing if the file was not selected', () => {
      const generator = openFile()
      // it opens the system dialog
      expect(generator.next().value).toEqual(
        call(electron.getFileOpenPath, jsonFileFilter)
      )
      // it stops
      expect(generator.next(null).done).toBeTruthy()
    })

    it('can process errors', () => {
      const error = new Error('some error message')

      const generator = openFile()
      generator.next()
      expect(generator.throw(error).value).toEqual(
        put(a.fileOpenError(error))
      )
    })
  })

  describe('saveAsFile', () => {
    it('saves a copy of the file with the system dialog', () => {
      const path = '/some/path'
      const state = { appData: { some: 'data' } }

      const generator = saveAsFile()
      // it opens the system dialog
      expect(generator.next().value).toEqual(
        call(electron.getFileSavePath, jsonFileFilter)
      )
      // it dispatches the `fileOpenStart` action
      expect(generator.next(path).value).toEqual(
        put(a.fileSaveAsStart())
      )
      // it reads the state
      expect(generator.next().value).toEqual(
        select()
      )
      // it saves a copy
      expect(generator.next(state).value).toEqual(
        call(electron.saveFile, path, electron.serializeData(state.appData))
      )
      // it dispatches the `fileSaveAsSuccess` action
      expect(generator.next().value).toEqual(
        put(a.fileSaveAsSuccess(
          path,
          electron.getFileName(path),
          state.appData
        ))
      )
    })

    it('do nothing if the file was not selected', () => {
      const generator = saveAsFile()
      // it opens the system dialog
      expect(generator.next().value).toEqual(
        call(electron.getFileSavePath, jsonFileFilter)
      )
      // it stops
      expect(generator.next(null).done).toBeTruthy()
    })

    it('can process errors', () => {
      const error = new Error('some error message')

      const generator = saveAsFile()
      generator.next()
      expect(generator.throw(error).value).toEqual(
        put(a.fileSaveAsError(error))
      )
    })
  })

  describe('saveFile', () => {
    it('saves the changes', () => {
      const state = {
        appData: { some: 'data' },
        manage: { filePath: '/some/path' }
      }

      const generator = saveFile()
      // it reads the state
      expect(generator.next().value).toEqual(
        select()
      )
      // it saves the file
      expect(generator.next(state).value).toEqual(
        call(
          electron.saveFile,
          state.manage.filePath,
          electron.serializeData(state.appData)
        )
      )
      // it dispatches the `fileSaveSuccess` action
      expect(generator.next().value).toEqual(
        put(a.fileSaveSuccess(state.appData))
      )
    })

    it('can process errors', () => {
      const error = new Error('some error message')

      const generator = saveFile()
      generator.next()
      expect(generator.throw(error).value).toEqual(
        put(a.fileSaveError(error))
      )
    })
  })

  describe('importChartData', () => {
    it('imports the chart data from the excel file', () => {
      const path = '/some/path'
      const data = '{ "some": "data" }'

      const generator = importChartData()
      // it opens the system dialog
      expect(generator.next().value).toEqual(
        call(electron.getFileOpenPath, excelFileFilter)
      )
      // it dispatches the `importStart` action
      expect(generator.next(path).value).toEqual(
        put(a.importStart())
      )
      // it reads the file
      expect(generator.next().value).toEqual(
        call(electron.importFromExcel, path)
      )
      // it dispatches the `importSuccess` action
      expect(generator.next(data).value).toEqual(
        put(a.importSuccess(data))
      )
    })

    it('do nothing if the file was not selected', () => {
      const generator = importChartData()
      // it opens the system dialog
      expect(generator.next().value).toEqual(
        call(electron.getFileOpenPath, excelFileFilter)
      )
      // it stops
      expect(generator.next(null).done).toBeTruthy()
    })

    it('can process errors', () => {
      const error = new Error('some error message')

      const generator = importChartData()
      generator.next()
      expect(generator.throw(error).value).toEqual(
        put(a.importError(error))
      )
    })
  })

  describe('quit', () => {
    it('quits the app', () => {
      const generator = quit()
      expect(generator.next().value).toEqual(
        call(electron.quit)
      )
    })
  })

  describe('manageSagas', () => {
    it('runs manage sagas', () => {
      const generator = manageSagas()
      expect(generator.next().value).toEqual(
        all([
          takeLatest(t.FILE_OPEN_REQUEST, openFile),
          takeLatest(t.FILE_SAVE_AS_REQUEST, saveAsFile),
          takeLatest(t.FILE_SAVE_START, saveFile),
          takeLatest(t.IMPORT_REQUEST, importChartData),
          takeLatest(t.QUIT, quit)
        ])
      )
    })
  })
})
