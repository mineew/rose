const webpack = require('webpack')

/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @param {Configuration} base
 * @returns {Configuration}
 */
module.exports = (base) => {
  const isProduction = base.mode === 'production'

  const moduleIdsPlugin = isProduction
    ? new webpack.HashedModuleIdsPlugin()
    : new webpack.NamedModulesPlugin()

  return {
    optimization: {
      runtimeChunk: 'single',
      splitChunks: {
        chunks: 'all',
        automaticNameDelimiter: '.'
      }
    },
    plugins: [moduleIdsPlugin]
  }
}
