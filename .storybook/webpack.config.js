module.exports = function ({ config }) {
  const prettierConfig = {
    printWidth: 80,
    tabWidth: 2,
    singleQuote: true,
    jsxSingleQuote: false,
    trailingComma: 'none',
    semi: false,
    bracketSpacing: true
  }

  const uglyCommentsRegex = [/^eslint-.*/, /^global.*/]

  config.module.rules.push({
    test: /\.stories\.jsx?$/,
    loaders: [{
      loader: require.resolve('@storybook/addon-storysource/loader'),
      options: {
        parser: 'javascript',
        injectDecorator: true,
        prettierConfig,
        uglyCommentsRegex
      }
    }],
    enforce: 'pre',
  })

  return config
}
