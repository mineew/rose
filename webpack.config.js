if (!['none', 'development', 'production'].includes(process.env.NODE_ENV)) {
  throw new Error('[Webpack]: Unknown mode.')
}

/// ----------------------------------------------------------------------------

const pt = require('path')
const flow = require('lodash.flow')
const merge = require('webpack-merge')

const parts = [
  require('./webpack/js'),
  require('./webpack/css'),
  require('./webpack/images'),
  require('./webpack/fonts'),
  require('./webpack/html'),
  require('./webpack/define'),
  require('./webpack/cache'),
  require('./webpack/devServer'),
  require('./webpack/stats'),
  require('./webpack/analyze')
]

/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @type {function(Configuration): Configuration}
 */
const extend = flow(parts.map(
  (part) => (base) => merge(base, part(base))
))

/// ----------------------------------------------------------------------------

const isProduction = process.env.NODE_ENV === 'production'

module.exports = extend({
  mode: process.env.NODE_ENV,
  bail: isProduction,
  entry: ['./src/index.js'],
  output: {
    path: pt.resolve(__dirname, 'build'),
    filename: `js/[name].[${isProduction ? 'contenthash' : 'hash'}].js`,
    publicPath: !isProduction ? '/' : undefined,
    hashDigestLength: 5
  },
  optimization: {
    minimize: isProduction
  }
})
