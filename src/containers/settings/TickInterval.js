import { NumberInput } from '../../components'
import { setTickInterval } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  NumberInput,
  (state) => state.tickInterval,
  setTickInterval,
  {
    label: 'Интервал сетки (градусы)',
    min: 1,
    max: 45
  }
)
