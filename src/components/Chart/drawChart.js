import Highcharts from 'highcharts'
import More from 'highcharts/highcharts-more'
import Exporting from 'highcharts/modules/exporting'

More(Highcharts)
Exporting(Highcharts)

let initialReflowed = false

/**
 * @typedef {import('./Chart').ChartProps} ChartProps
 */

/**
 * @param {HTMLElement} element
 * @param {ChartProps} props
 * @returns {void}
 */
export default function drawChart (element, props) {
  const chart = Highcharts.chart(element, {
    ...generalOptions(),
    ...titleOptions(props),
    ...xAxisOptions(props),
    ...yAxisOptions(props),
    ...plotOptions(props),
    ...seriesOptions(props)
  })
  setTimeout(() => {
    !initialReflowed && chart.reflow()
    initialReflowed = true
  }, 0)
}

/**
 * @param {string} angle
 * @returns {boolean}
 */
export function angleIsValid (angle) {
  const clearAngle = angle.trim()
  const numberAngle = Number(clearAngle)

  return (clearAngle !== '') && (
    numberAngle >= 0 && numberAngle < 360
  )
}

/**
 * @param {string} value
 * @returns {boolean}
 */
export function valueIsValid (value) {
  const clearValue = value.trim()
  const numberValue = Number(clearValue)

  return clearValue !== '' && !Number.isNaN(numberValue)
}

/**
 * @param {Array<[string, string]>} rawData
 * @returns {Array<[number, number]>}
 */
export function prepareChartData (rawData) {
  const isValid = ([ a, v ]) => angleIsValid(a) && valueIsValid(v)
  const toNumbers = ([ a, v ]) => [Number(a), Number(v)]
  const asceding = (pointA, pointB) => pointA[0] - pointB[0]

  const clearData = rawData
    .filter(isValid)
    .map(toNumbers)

  return collapseDataPoints(clearData)
    .map(transformDataPoint)
    .sort(asceding)
}

/// ----------------------------------------------------------------------------
/// Helper functions for building Highcharts options

/**
 * @private
 * @returns {Highcharts.Options}
 */
function generalOptions () {
  return {
    chart: {
      polar: true,
      animation: false
    },
    pane: {
      startAngle: -90,
      endAngle: 90
    },
    tooltip: {
      enabled: false
    }
  }
}

/**
 * @private
 * @param {ChartProps} props
 * @returns {Highcharts.Options}
 */
function titleOptions (props) {
  const { title, subtitle } = props

  return {
    title: {
      text: title
    },
    subtitle: {
      text: subtitle
    }
  }
}

/**
 * @private
 * @param {ChartProps} props
 * @returns {Highcharts.Options}
 */
function xAxisOptions (props) {
  const { tickInterval, step, labelsSize, labelsColor, gridColor } = props

  return {
    xAxis: {
      min: 0,
      max: 180,
      tickInterval,
      labels: {
        step,
        formatter () {
          return formatAngle(this.value)
        },
        style: {
          fontSize: `${labelsSize}px`,
          color: labelsColor
        }
      },
      lineColor: gridColor,
      gridLineColor: gridColor,
      showLastLabel: true
    }
  }
}

/**
 * @private
 * @param {ChartProps} props
 * @returns {Highcharts.Options}
 */
function yAxisOptions (props) {
  const { labelsSize, labelsColor, gridColor } = props

  return {
    yAxis: {
      min: 0,
      labels: {
        align: 'center',
        y: 20,
        formatter () {
          return this.value
        },
        style: {
          fontSize: `${labelsSize}px`,
          color: labelsColor
        }
      },
      lineColor: gridColor,
      gridLineColor: gridColor
    }
  }
}

/**
 * @private
 * @param {ChartProps} props
 * @returns {Highcharts.Options}
 */
function plotOptions (props) {
  const {
    tickInterval,
    linesColor,
    linesWidth,
    areasBgColor,
    areasOpacity,
    areasLineColor,
    areasLineWidth
  } = props

  return {
    plotOptions: {
      series: {
        pointStart: 0,
        pointInterval: tickInterval,
        states: {
          hover: {
            enabled: false
          }
        },
        animation: false
      },
      line: {
        marker: {
          enabled: false
        },
        color: linesColor,
        lineWidth: linesWidth
      },
      area: {
        marker: {
          enabled: false
        },
        color: areasBgColor,
        fillOpacity: areasOpacity / 100,
        lineColor: areasLineColor,
        lineWidth: areasLineWidth
      }
    }
  }
}

/**
 * @private
 * @param {ChartProps} props
 * @returns {Highcharts.Options}
 */
function seriesOptions (props) {
  const { data, showLines, showAreas } = props

  const lines = getLineSeries(data)
  const areas = getAreaSeries(data)

  const series = []

  if (showLines && lines.length) {
    series.push(...lines)
  }

  if (showAreas && areas.length) {
    series.push(...areas)
  }

  if (!series.length) {
    series.push(...getEmptySeries())
  }

  return {
    series
  }
}

/// ----------------------------------------------------------------------------
/// Helper functions for formatting data

/**
 * @private
 * @param {number} angle
 * @returns {string}
 */
function formatAngle (angle) {
  if (angle >= 90) {
    return `${angle - 90}°`
  } else {
    return `${angle + 270}°`
  }
}

/**
 * @private
 * @param {Array<[number, number]>} data
 * @returns {Array<Highcharts.SeriesLineOptions>}
 */
function getLineSeries (data) {
  if (!data.length) {
    return []
  }

  /** @type {Array<Highcharts.SeriesLineOptions>} */
  const series = []

  for (let point of data) {
    series.push({
      type: 'line',
      data: [[0, 0], point],
      showInLegend: false
    })
  }

  return series
}

/**
 * @private
 * @param {Array<[number, number]>} data
 * @returns {Array<Highcharts.SeriesAreaOptions>}
 */
function getAreaSeries (data) {
  if (!data.length) {
    return []
  }

  return [{
    type: 'area',
    data: [[0, 0], ...data],
    showInLegend: false
  }]
}

/**
 * @private
 * @returns {Array<Highcharts.SeriesLineOptions>}
 */
function getEmptySeries () {
  return [{
    showInLegend: false
  }]
}

/**
 * @private
 * @param {[number, number]} point
 * @returns {[number, number]}
 */
function transformDataPoint (point) {
  const [ angle, value ] = point

  if (angle >= 0 && angle <= 90) {
    return [angle + 90, value]
  }

  if (angle >= 270 && angle <= 359) {
    return [angle - 270, value]
  }

  return point
}

/**
 * @private
 * @param {[number, number]} point
 * @returns {boolean}
 */
function dataPointIsValid (point) {
  const [ angle ] = point

  return (angle >= 0 && angle <= 90) ||
    (angle >= 270 && angle < 360)
}

/**
 * @private
 * @param {Array<[number, number]>} points
 * @returns {Array<[number, number]>}
 */
function collapseDataPoints (points) {
  const validPoints = points.filter((p) => dataPointIsValid(p))
  const invalidPoints = points.filter((p) => !dataPointIsValid(p))

  for (let i = 0; i < invalidPoints.length; i++) {
    for (let j = 0; j < validPoints.length; j++) {
      const [ invalidAngle, invalidPointValue ] = invalidPoints[i]
      const [ validAngle ] = validPoints[j]

      if ([invalidAngle - 180, invalidAngle + 180].includes(validAngle)) {
        validPoints[j][1] += invalidPointValue
      }
    }
  }

  return validPoints
}
