import * as t from '../actions/types'

export const initialState = {
  chartData: [],
  settings: {
    title: '',
    subtitle: '',
    tickInterval: 5,
    step: 3,
    labelsSize: 11,
    labelsColor: '#666666',
    gridColor: '#e6e6e6',
    showLines: true,
    linesColor: '#ff0000',
    linesWidth: 1,
    showAreas: true,
    areasBgColor: '#ff0000',
    areasOpacity: 50,
    areasLineColor: '#ff0000',
    areasLineWidth: 1
  }
}

export default function appData (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case t.SET_DATA_POINTS: {
      return {
        ...state,
        chartData: [].concat(payload)
      }
    }

    case t.SET_TITLE: {
      return setSettings(state, 'title', payload)
    }

    case t.SET_SUBTITLE: {
      return setSettings(state, 'subtitle', payload)
    }

    case t.SET_TICK_INTERVAL: {
      return setSettings(state, 'tickInterval', payload)
    }

    case t.SET_STEP: {
      return setSettings(state, 'step', payload)
    }

    case t.SET_LABELS_SIZE: {
      return setSettings(state, 'labelsSize', payload)
    }

    case t.SET_LABELS_COLOR: {
      return setSettings(state, 'labelsColor', payload)
    }

    case t.SET_GRID_COLOR: {
      return setSettings(state, 'gridColor', payload)
    }

    case t.TOOGLE_SHOW_LINES: {
      return setSettings(state, 'showLines', !state.settings.showLines)
    }

    case t.SET_LINES_COLOR: {
      return setSettings(state, 'linesColor', payload)
    }

    case t.SET_LINES_WIDTH: {
      return setSettings(state, 'linesWidth', payload)
    }

    case t.TOOGLE_SHOW_AREAS: {
      return setSettings(state, 'showAreas', !state.settings.showAreas)
    }

    case t.SET_AREAS_BG_COLOR: {
      return setSettings(state, 'areasBgColor', payload)
    }

    case t.SET_AREAS_OPACITY: {
      return setSettings(state, 'areasOpacity', payload)
    }

    case t.SET_AREAS_LINE_COLOR: {
      return setSettings(state, 'areasLineColor', payload)
    }

    case t.SET_AREAS_LINE_WIDTH: {
      return setSettings(state, 'areasLineWidth', payload)
    }

    case t.FILE_OPEN_SUCCESS: {
      return payload.data
    }

    case t.FILE_CLOSE: {
      return initialState
    }

    case t.IMPORT_SUCCESS: {
      return {
        ...initialState,
        chartData: payload.chartData
      }
    }

    default: {
      return state
    }
  }
}

/// ----------------------------------------------------------------------------

function setSettings (state, prop, value) {
  return {
    ...state,
    settings: {
      ...state.settings,
      [prop]: value
    }
  }
}
