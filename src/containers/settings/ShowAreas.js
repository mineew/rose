import { CheckInput } from '../../components'
import { toogleShowAreas } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  CheckInput,
  (state) => state.showAreas,
  toogleShowAreas,
  {
    label: 'Показывать область'
  }
)
