import React from 'react'
import { FileMenu } from '../../containers'
import './Menu.css'

function Menu () {
  return (
    <div className="Menu">
      <FileMenu />
    </div>
  )
}

export default Menu
