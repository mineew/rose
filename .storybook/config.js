import { configure, addParameters, addDecorator } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'
import '@storybook/addon-console'
import theme from './theme'

import 'antd/dist/antd.css'

addParameters({
  options: {
    theme
  }
})

addDecorator(withKnobs)

function loadStories () {
  const customRequire = require.context('../src', true, /\.stories\.js$/)
  customRequire.keys().forEach((filename) => customRequire(filename))
}

configure(loadStories, module)
