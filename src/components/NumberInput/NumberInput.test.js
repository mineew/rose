import React from 'react'
import { render } from 'react-testing-library'
import NumberInput from './NumberInput'

describe('<NumberInput />', () => {
  const min = 0
  const max = 100
  const value = 50
  const onChange = jest.fn()
  const label = 'Number Input Label'

  const wrapper = (props = {}) => render(
    <NumberInput
      min={min}
      max={max}
      value={value}
      onChange={onChange}
      label={label}
      {...props}
    />
  )

  it('renders slider', () => {
    const { baseElement } = wrapper()
    expect(baseElement.outerHTML).toMatch(`aria-valuemin="${min}"`)
    expect(baseElement.outerHTML).toMatch(`aria-valuemax="${max}"`)
    expect(baseElement.outerHTML).toMatch(`aria-valuenow="${value}"`)
  })

  it('takes the `label` prop', () => {
    const { baseElement } = wrapper()
    expect(baseElement.innerHTML).toMatch(label)
  })
})
