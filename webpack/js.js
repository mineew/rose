const webpack = require('webpack')
const TerserWebpackPlugin = require('terser-webpack-plugin')

/**
 * @typedef {import('webpack').Configuration} Configuration
 */

/**
 * @param {Configuration} base
 * @returns {Configuration}
 */
module.exports = (base) => {
  const isProduction = base.mode === 'production'
  const hashLength = base.output.hashDigestLength

  const babel = {
    loader: 'babel-loader'
  }

  const minimizePlugin = isProduction && new TerserWebpackPlugin({
    sourceMap: true,
    cache: true,
    parallel: true
  })

  const devtoolPlugin = new webpack.SourceMapDevToolPlugin({
    //
    // source maps can be excluded from the vendors bundle if necessary
    // but browsers display an error in the console that the maps are not found
    // TODO: need to figure out how to avoid this error
    //
    // exclude: /(vendors|runtime)/,
    filename: `js/[name].[contenthash:${hashLength}].js.map`
  })

  return {
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: [babel]
      }]
    },
    optimization: {
      minimizer: [minimizePlugin].filter(Boolean)
    },
    devtool: false,
    plugins: [devtoolPlugin]
  }
}
