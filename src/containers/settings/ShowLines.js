import { CheckInput } from '../../components'
import { toogleShowLines } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  CheckInput,
  (state) => state.showLines,
  toogleShowLines,
  {
    label: 'Показывать линии'
  }
)
