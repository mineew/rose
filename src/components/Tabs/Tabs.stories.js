import React from 'react'
import { storiesOf } from '@storybook/react'
import { object } from '@storybook/addon-knobs'
import Tabs from './Tabs'

const stories = storiesOf('Components', module)

stories.add('Tabs', () => {
  const tabsKnob = object('tabs', [{
    title: 'Data',
    icon: 'database',
    content: 'Data'
  }, {
    title: 'Settings',
    icon: 'setting',
    content: 'Settings'
  }])

  return (
    <div style={{ margin: 20 }}>
      <Tabs tabs={tabsKnob} />
    </div>
  )
})
