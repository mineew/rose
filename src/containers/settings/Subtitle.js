import { TextInput } from '../../components'
import { setSubtitle } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  TextInput,
  (state) => state.subtitle,
  setSubtitle,
  {
    label: 'Подзаголовок'
  }
)
