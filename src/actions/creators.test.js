import { isFSA } from 'flux-standard-action'
import * as t from './types'
import * as a from './creators'

describe('app data actions', () => {
  describe('setDataPoints', () => {
    const dataPoints = [[1, 1], [2, 2], [3, 3], [4, 4], [5, 5]]
    const created = a.setDataPoints(dataPoints)
    const expected = { type: t.SET_DATA_POINTS, payload: dataPoints }

    it('creates `SET_DATA_POINTS` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setTitle', () => {
    const title = 'Title'
    const created = a.setTitle(title)
    const expected = { type: t.SET_TITLE, payload: title }

    it('creates `SET_TITLE` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setSubtitle', () => {
    const subtitle = 'Subtitle'
    const created = a.setSubtitle(subtitle)
    const expected = { type: t.SET_SUBTITLE, payload: subtitle }

    it('creates `SET_SUBTITLE` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setTickInterval', () => {
    const tickInterval = 5
    const created = a.setTickInterval(tickInterval)
    const expected = { type: t.SET_TICK_INTERVAL, payload: tickInterval }

    it('creates `SET_TICK_INTERVAL` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setStep', () => {
    const step = 3
    const created = a.setStep(step)
    const expected = { type: t.SET_STEP, payload: step }

    it('creates `SET_STEP` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setLabelsSize', () => {
    const labelsSize = 11
    const created = a.setLabelsSize(labelsSize)
    const expected = { type: t.SET_LABELS_SIZE, payload: labelsSize }

    it('creates `SET_LABELS_SIZE` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setLabelsColor', () => {
    const labelsColor = '#f5f5f5'
    const created = a.setLabelsColor(labelsColor)
    const expected = { type: t.SET_LABELS_COLOR, payload: labelsColor }

    it('creates `SET_LABELS_COLOR` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setGridColor', () => {
    const gridColor = '#f5f5f5'
    const created = a.setGridColor(gridColor)
    const expected = { type: t.SET_GRID_COLOR, payload: gridColor }

    it('creates `SET_GRID_COLOR` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('toogleShowLines', () => {
    const created = a.toogleShowLines()
    const expected = { type: t.TOOGLE_SHOW_LINES }

    it('creates `TOOGLE_SHOW_LINES` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setLinesColor', () => {
    const linesColor = '#f5f5f5'
    const created = a.setLinesColor(linesColor)
    const expected = { type: t.SET_LINES_COLOR, payload: linesColor }

    it('creates `SET_LINES_COLOR` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setLinesWidth', () => {
    const linesWidth = 1
    const created = a.setLinesWidth(linesWidth)
    const expected = { type: t.SET_LINES_WIDTH, payload: linesWidth }

    it('creates `SET_LINES_WIDTH` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('toogleShowAreas', () => {
    const created = a.toogleShowAreas()
    const expected = { type: t.TOOGLE_SHOW_AREAS }

    it('creates `TOOGLE_SHOW_AREAS` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setAreasBgColor', () => {
    const areasBgColor = '#f5f5f5'
    const created = a.setAreasBgColor(areasBgColor)
    const expected = { type: t.SET_AREAS_BG_COLOR, payload: areasBgColor }

    it('creates `SET_AREAS_BG_COLOR` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setAreasOpacity', () => {
    const areasOpacity = 50
    const created = a.setAreasOpacity(areasOpacity)
    const expected = { type: t.SET_AREAS_OPACITY, payload: areasOpacity }

    it('creates `SET_AREAS_OPACITY` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setAreasLineColor', () => {
    const areasLineColor = '#f5f5f5'
    const created = a.setAreasLineColor(areasLineColor)
    const expected = { type: t.SET_AREAS_LINE_COLOR, payload: areasLineColor }

    it('creates `SET_AREAS_LINE_COLOR` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('setAreasLineWidth', () => {
    const areasLineWidth = 1
    const created = a.setAreasLineWidth(areasLineWidth)
    const expected = { type: t.SET_AREAS_LINE_WIDTH, payload: areasLineWidth }

    it('creates `SET_AREAS_LINE_WIDTH` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })
})

describe('manage actions', () => {
  describe('fileOpenRequest', () => {
    const created = a.fileOpenRequest()
    const expected = { type: t.FILE_OPEN_REQUEST }

    it('creates `FILE_OPEN_REQUEST` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileOpenStart', () => {
    const created = a.fileOpenStart()
    const expected = { type: t.FILE_OPEN_START }

    it('creates `FILE_OPEN_START` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileOpenSuccess', () => {
    const path = '/path/to/a/file'
    const name = 'file'
    const data = 'some data'
    const created = a.fileOpenSuccess(path, name, data)
    const expected = {
      type: t.FILE_OPEN_SUCCESS,
      payload: { path, name, data }
    }

    it('creates `FILE_OPEN_SUCCESS` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileOpenError', () => {
    const error = new Error('some error')
    const created = a.fileOpenError(error)
    const expected = {
      type: t.FILE_OPEN_ERROR,
      payload: error,
      error: true
    }

    it('creates `FILE_OPEN_ERROR` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileSaveAsRequest', () => {
    const created = a.fileSaveAsRequest()
    const expected = { type: t.FILE_SAVE_AS_REQUEST }

    it('creates `FILE_SAVE_AS_REQUEST` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileSaveAsStart', () => {
    const created = a.fileSaveAsStart()
    const expected = { type: t.FILE_SAVE_AS_START }

    it('creates `FILE_SAVE_AS_START` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileSaveAsSuccess', () => {
    const path = '/path/to/a/file'
    const name = 'file'
    const data = 'some data'
    const created = a.fileSaveAsSuccess(path, name, data)
    const expected = {
      type: t.FILE_SAVE_AS_SUCCESS,
      payload: { path, name, data }
    }

    it('creates `FILE_SAVE_AS_SUCCESS` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileSaveAsError', () => {
    const error = new Error('some error')
    const created = a.fileSaveAsError(error)
    const expected = {
      type: t.FILE_SAVE_AS_ERROR,
      payload: error,
      error: true
    }

    it('creates `FILE_SAVE_AS_ERROR` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileSaveStart', () => {
    const created = a.fileSaveStart()
    const expected = { type: t.FILE_SAVE_START }

    it('creates `FILE_SAVE_START` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileSaveSuccess', () => {
    const data = 'some data'
    const created = a.fileSaveSuccess(data)
    const expected = {
      type: t.FILE_SAVE_SUCCESS,
      payload: { data }
    }

    it('creates `FILE_SAVE_SUCCESS` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileSaveError', () => {
    const error = new Error('some error')
    const created = a.fileSaveError(error)
    const expected = {
      type: t.FILE_SAVE_ERROR,
      payload: error,
      error: true
    }

    it('creates `FILE_SAVE_ERROR` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('fileClose', () => {
    const created = a.fileClose()
    const expected = { type: t.FILE_CLOSE }

    it('creates `FILE_CLOSE` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('importRequest', () => {
    const created = a.importRequest()
    const expected = { type: t.IMPORT_REQUEST }

    it('creates `IMPORT_REQUEST` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('importStart', () => {
    const created = a.importStart()
    const expected = { type: t.IMPORT_START }

    it('creates `IMPORT_START` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('importSuccess', () => {
    const chartData = [['90', '1200'], ['0', '1212'], ['180', '1253']]
    const created = a.importSuccess(chartData)
    const expected = {
      type: t.IMPORT_SUCCESS,
      payload: { chartData }
    }

    it('creates `IMPORT_SUCCESS` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('importError', () => {
    const error = new Error('some error')
    const created = a.importError(error)
    const expected = {
      type: t.IMPORT_ERROR,
      payload: error,
      error: true
    }

    it('creates `IMPORT_ERROR` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })

  describe('quit', () => {
    const created = a.quit()
    const expected = { type: t.QUIT }

    it('creates `QUIT` action', () => {
      expect(created).toEqual(expected)
    })

    it('is FSA compliant', () => {
      expect(isFSA(created)).toBeTruthy()
    })
  })
})
