import {
  fileOpenRequest,
  fileSaveAsRequest,
  fileSaveStart,
  fileClose,
  importRequest,
  quit
} from '../../actions'
import { mapStateToProps, mapDispatchToProps } from './FileMenu'

describe('<FileMenu /> container', () => {
  it('maps `fileName` from state and checks if there is unsaved data', () => {
    const state = {
      manage: {
        fileName: 'some-file.ext',
        lastSavedAppData: 'data'
      },
      appData: 'new data'
    }
    const props = {
      fileName: state.manage.fileName,
      needSave: state.manage.lastSavedAppData !== state.appData
    }
    expect(mapStateToProps(state)).toEqual(props)
  })

  it('maps menu callbacks to actions', () => {
    const dispatch = jest.fn()
    const props = mapDispatchToProps(dispatch)

    props.onFileCloseClick()
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch).toHaveBeenLastCalledWith(fileClose())

    props.onFileOpenClick()
    expect(dispatch).toHaveBeenCalledTimes(2)
    expect(dispatch).toHaveBeenLastCalledWith(fileOpenRequest())

    props.onFileSaveClick()
    expect(dispatch).toHaveBeenCalledTimes(3)
    expect(dispatch).toHaveBeenLastCalledWith(fileSaveStart())

    props.onImportClick()
    expect(dispatch).toHaveBeenCalledTimes(4)
    expect(dispatch).toHaveBeenLastCalledWith(importRequest())

    props.onQuitClick()
    expect(dispatch).toHaveBeenCalledTimes(5)
    expect(dispatch).toHaveBeenLastCalledWith(quit())

    props.onSaveAsClick()
    expect(dispatch).toHaveBeenCalledTimes(6)
    expect(dispatch).toHaveBeenLastCalledWith(fileSaveAsRequest())
  })
})
