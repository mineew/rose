import React from 'react'
import PropTypes from 'prop-types'
import { Collapse, Icon } from 'antd'
import './CollapseCard.css'

/**
 * @typedef {Object} CollapseCardProps
 * @property {string} title
 * @property {React.ReactNode} children
 * @property {string} [icon]
 */

/**
 * @param {CollapseCardProps} props
 * @returns {React.Component<CollapseCardProps>}
 */
function CollapseCard (props) {
  const { title, children, icon } = props

  const extra = icon
    ? <Icon type={icon} />
    : undefined

  return (
    <Collapse
      className="CollapseCard"
      defaultActiveKey={title}
    >
      <Collapse.Panel
        key={title}
        header={title}
        extra={extra}
      >
        {children}
      </Collapse.Panel>
    </Collapse>
  )
}

CollapseCard.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  icon: PropTypes.string
}

export default CollapseCard
