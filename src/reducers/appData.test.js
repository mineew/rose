import deepFreeze from 'deep-freeze'
import * as t from '../actions/types'
import appData, { initialState } from './appData'

describe('appData reducer', () => {
  it('returns the initial state', () => {
    const reducedState = appData(undefined, { type: 'SOME_UNEXPECTED_ACTION' })
    expect(reducedState).toEqual(initialState)
  })

  it('handles `SET_DATA_POINTS` action', () => {
    const action = {
      type: t.SET_DATA_POINTS,
      payload: [['1', '1'], ['2', '2'], ['3', '3']]
    }
    const stateBefore = initialState
    const stateAfter = { ...initialState, chartData: action.payload }

    deepFreeze(stateBefore)

    const reducedState = appData(stateBefore, action)
    expect(reducedState).toEqual(stateAfter)
  })

  it('handles `SET_TITLE` action', () => {
    testSettingsReducer(t.SET_TITLE, 'title', 'new title')
  })

  it('handles `SET_SUBTITLE` action', () => {
    testSettingsReducer(t.SET_SUBTITLE, 'subtitle', 'new subtitle')
  })

  it('handles `SET_TICK_INTERVAL` action', () => {
    testSettingsReducer(t.SET_TICK_INTERVAL, 'tickInterval', 100)
  })

  it('handles `SET_STEP` action', () => {
    testSettingsReducer(t.SET_STEP, 'step', 100)
  })

  it('handles `SET_LABELS_SIZE` action', () => {
    testSettingsReducer(t.SET_LABELS_SIZE, 'labelsSize', 100)
  })

  it('handles `SET_LABELS_COLOR` action', () => {
    testSettingsReducer(t.SET_LABELS_COLOR, 'labelsColor', 'blue')
  })

  it('handles `SET_GRID_COLOR` action', () => {
    testSettingsReducer(t.SET_GRID_COLOR, 'gridColor', 'blue')
  })

  it('handles `TOOGLE_SHOW_LINES` action', () => {
    testSettingsReducer(
      t.TOOGLE_SHOW_LINES,
      'showLines',
      !initialState.settings.showLines
    )
  })

  it('handles `SET_LINES_COLOR` action', () => {
    testSettingsReducer(t.SET_LINES_COLOR, 'linesColor', 'blue')
  })

  it('handles `SET_LINES_WIDTH` action', () => {
    testSettingsReducer(t.SET_LINES_WIDTH, 'linesWidth', 100)
  })

  it('handles `TOOGLE_SHOW_AREAS` action', () => {
    testSettingsReducer(
      t.TOOGLE_SHOW_AREAS,
      'showAreas',
      !initialState.settings.showAreas
    )
  })

  it('handles `SET_AREAS_BG_COLOR` action', () => {
    testSettingsReducer(t.SET_AREAS_BG_COLOR, 'areasBgColor', 'blue')
  })

  it('handles `SET_AREAS_OPACITY` action', () => {
    testSettingsReducer(t.SET_AREAS_OPACITY, 'areasOpacity', 75)
  })

  it('handles `SET_AREAS_LINE_COLOR` action', () => {
    testSettingsReducer(t.SET_AREAS_LINE_COLOR, 'areasLineColor', 'blue')
  })

  it('handles `SET_AREAS_LINE_WIDTH` action', () => {
    testSettingsReducer(t.SET_AREAS_LINE_WIDTH, 'areasLineWidth', 100)
  })

  it('handles `FILE_OPEN_SUCCESS` action', () => {
    const action = {
      type: t.FILE_OPEN_SUCCESS,
      payload: {
        data: {
          ...initialState,
          chartData: [['1', '1'], ['2', '2'], ['3', '3']]
        }
      }
    }
    const stateBefore = initialState
    const stateAfter = action.payload.data

    deepFreeze(stateBefore)

    const reducedState = appData(stateBefore, action)
    expect(reducedState).toEqual(stateAfter)
  })

  it('handles `FILE_CLOSE` action', () => {
    const action = { type: t.FILE_CLOSE }
    const stateBefore = initialState
    const stateAfter = initialState

    deepFreeze(stateBefore)

    const reducedState = appData(stateBefore, action)
    expect(reducedState).toEqual(stateAfter)
  })

  it('handles `IMPORT_SUCCESS` action', () => {
    const action = {
      type: t.IMPORT_SUCCESS,
      payload: {
        chartData: [['1', '1'], ['2', '2'], ['3', '3']]
      }
    }
    const stateBefore = initialState
    const stateAfter = { ...initialState, chartData: action.payload.chartData }

    deepFreeze(stateBefore)

    const reducedState = appData(stateBefore, action)
    expect(reducedState).toEqual(stateAfter)
  })
})

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {string} actionType
 * @param {string} key
 * @param {string|number|boolean} value
 */
function testSettingsReducer (actionType, key, value) {
  const action = {
    type: actionType,
    payload: value
  }
  const stateBefore = initialState
  const stateAfter = {
    ...initialState,
    settings: { ...initialState.settings }
  }
  stateAfter.settings[key] = value

  deepFreeze(stateBefore)

  const reducedState = appData(stateBefore, action)
  expect(reducedState).toEqual(stateAfter)
}
