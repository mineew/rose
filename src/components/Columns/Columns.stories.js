import React from 'react'
import { storiesOf } from '@storybook/react'
import { number } from '@storybook/addon-knobs'
import Columns from './Columns'

const stories = storiesOf('Components', module)

const left = `
But I must explain to you how all this mistaken idea of denouncing pleasure and
praising pain was born and I will give you a complete account of the system,
and expound the actual teachings of the great explorer of the truth, the
master-builder of human happiness.
`.trim()

const right = `
On the other hand, we denounce with righteous indignation and dislike men who
are so beguiled and demoralized by the charms of pleasure of the moment, so
blinded by desire, that they cannot foresee the pain and trouble that are bound
to ensue; and equal blame belongs to those who fail in their duty through
weakness of will, which is the same as saying through shrinking from toil and
pain.
`.trim()

stories.add('Columns', () => {
  const leftColumnWidthKnob = number(
    'leftColumnWidth',
    Columns.defaultProps.leftColumnWidth
  )

  return (
    <div style={{ margin: 20 }}>
      <Columns leftColumnWidth={leftColumnWidthKnob}>
        <p>{left}</p>
        <p>{right}</p>
      </Columns>
    </div>
  )
})
