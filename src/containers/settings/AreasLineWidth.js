import { NumberInput } from '../../components'
import { setAreasLineWidth } from '../../actions'
import connectSettingsInput from './connectSettingsInput'

export default connectSettingsInput(
  NumberInput,
  (state) => state.areasLineWidth,
  setAreasLineWidth,
  {
    label: 'Толщина обводки области',
    min: 0,
    max: 10
  }
)
